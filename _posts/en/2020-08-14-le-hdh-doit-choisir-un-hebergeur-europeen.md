---
layout: post
title: "End of Privacy Shield: Health Data Hub must choose a European hosting provider"
ref: le-hdh-doit-choisir-un-hebergeur-europeen
lang: en
show_comments: true
---

On July 31, 2020 the French Data Regulator (CNIL) published the first information on the consequences of the invalidation of the Privacy Shield. They are the word-for-word translation of the FAQ published by the European Data Protection Board [^invalidprivacy].

The summary is simple: "the requirements of US law [...] result in limitations on the protection of personal data that are not circumscribed so as to meet requirements that are substantially equivalent to those required by EU law". To put it plainly, "**any transfer of data to the US presents a risk**" [^usinedig].

<!-- more -->

### Let's go back to the Privacy Shield

The "Privacy Shield" is a text adopted in 2016 that allowed digital companies to legally transfer personal data (identity, online behaviour, geolocation...) of European citizens to the United States.

This text was attacked by "Mr Maximillian Schrems, a Facebook user since 2008.  As for other users residing in the Union, Mr. Schrems' personal data are, in whole or in part, transferred by Facebook Ireland to servers belonging to Facebook Inc. located in the United States, where they are processed. Mr Schrems has lodged a complaint with the Irish supervisory authority, seeking, in substance, to prohibit those transfers. He argued that US law and practice do not provide sufficient protection against access by public authorities to data transferred to the United States"[^CJUE_PrivacyShield].

Here is the answer of the European Court of Justice ECJ. "The limitations on the protection of personal data arising from the internal rules of the United States concerning the access and use by the US public authorities of such data transferred from the [European] Union to that third country are not framed in such a way as to meet requirements equivalent to those required under EU law by the principle of proportionality, in that the monitoring programmes based on those rules are not limited to what is strictly necessary. On the basis of the findings contained in that decision, the ECJ notes that, **for certain monitoring programmes, the said rules do not in any way reveal the existence of limitations on the authorisation which they contain for the implementation of those programmes, nor the existence of safeguards for non-US persons potentially targeted**"[^CJEU_PrivacyShield].

U.S. monitoring programs have no limits on the use of data. The ECJ therefore invalidates the global agreement that the Privacy Shield constituted. 

On the other hand, it confirms that companies can comply with European law by committing themselves individually to respect certain precautions regarding the use of their European users' data, via the Standard Contractual Clauses (SCCs). In this case a "level of protection equivalent to that guaranteed within the European Union by the GDPR" is required. Otherwise transfers must be suspended or inderdit [^invalidprivacy].

Mr Schrems welcomed this decision by announcing "a fundamental change that goes far beyond data transfers between the EU and the US. Authorities such as the Irish Data Protection Commission have undermined the success of the DPMR so far. The court makes it clear that they have to get down to work and enforce the law" [^CJEU_lemonde].

Following this ruling, the German CNIL stresses that "data should not be transferred to the United States until this legal framework has been reformed"[^regulateurallemand].


### What about our health data?

The Health Data Hub or "Health Data Platform" brings together all the health data of more than 67 million people at Microsoft Azure, the American giant's cloud.
This data is the most sensitive of personal data.

The CNIL [^CNILdel] has also specified that in view of the sensitivity of the data in question, it would like the hosting and services related to the management of the Health Data Hub to be reserved for entities under the exclusive jurisdiction of the European Union. 
The Agence nationale de la sécurité des systèmes d'information (ANSSI), through its Director General Guillaume POUPARD [^poupard], also specified that it would be preferable to favour a European solution not subject to extra-European laws.
Recently the Council of State [^conseil_etat] pointed out that the Platform's data could transit through the United States when they are processed.

At the hearing at the Council of State on 11 June, we learned that although Microsoft could guarantee the location of data storage, this was not the case for analysis data. Analysis data, once copied to Microsoft's processors, circulates around the world. We are not talking about maintenance data here, which is not health data.

On July 2nd the European Data Protection Supervisor issued a report [^CEPD_produits_microsoft] on Microsoft products. The lack of information and control over analysis data was clearly pointed out and was "of concern".  It was also pointed out that "**EU institutions have not been able to control the location of much of the data processed by Microsoft**" and that "**Microsoft discloses personal data** (including customer data, administrator data, payment data and support data) to third parties, including law enforcement or other government agencies".

It is now essential to use providers that are not subject to the extraterritoriality of US law: Patriot Act and CLOUD Act. CTCs are by no means sufficient to prevent access to data by US authorities, especially in the context of US surveillance programmes.

We therefore assert that the hosting of the Health Data Platform, in order to comply with the decision of the ECJ, must be entrusted to a European operator. The ECCs requires requirements equivalent to those of European Union law. At this stage, "any transfer to the United States carries a risk" and "transfers carried out on the basis of this legal framework are illegal".

#### Sources

[^CJUE_PrivacyShield]: [La Cour invalide la décision 2016/1250 relative à l'adéquation de la protection assurée par le bouclier de protection des données UE-États-Unis](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf)

[^CEPD_produits_microsoft]: [Résultats de l’enquête par les institutions européennes concernant l’utilisation des produits et services Microsoft](https://pad.interhop.org/itdQNkLlT8GEG08yUIspVQ#)

[^invalidprivacy]: [Invalidation du « Privacy shield » : les premières questions-réponses du CEPD](https://www.cnil.fr/fr/invalidation-du-privacy-shield-les-premieres-questions-reponses-du-cepd)

[^linkedun]: https://www.linkedin.com/feed/update/urn:li:activity:6695272380106870784/

[^usinedig]: [L'annulation du Privacy Shield doit s'appliquer immédiatement, tranche la Cnil européenne ](https://www.usine-digitale.fr/article/l-annulation-du-privacy-shield-doit-s-appliquer-immediatement-tranche-la-cnil-europeenne.N989069)

[^CJUE_lemonde]: [L’accord sur le transfert de données personnelles entre l’UE et les Etats-Unis annulé par la justice européenne](https://www.lemonde.fr/pixels/article/2020/07/16/la-justice-europeenne-annule-l-accord-sur-le-transfert-de-donnees-personnelles-ue-etats-unis_6046344_4408996.html)

[^regulateurallemand]: [https://www.dataguidance.com/news/berlin-berlin-commissioner-issues-statement-schrems-ii-case-asks-controllers-stop-data](https://www.dataguidance.com/news/berlin-berlin-commissioner-issues-statement-schrems-ii-case-asks-controllers-stop-data)

[^poupard]: [Office parlementaire d'évaluation des choix scientifiques et technologiques](https://videos.senat.fr/video.1608918_5ebaa04d6b8cd.audition-de-m-guillaume-poupard-directeur-general-de-l-agence-nationale-de-la-securite-des-systeme?timecode=4319000)

[^conseil_etat]: [Conseil d'État, 19 juin 2020, Plateforme Health Data Hub](https://www.conseil-etat.fr/ressources/decisions-contentieuses/dernieres-decisions-importantes/conseil-d-etat-19-juin-2020-plateforme-health-data-hub)

[^CNILdel]: [Mesures d’organisation et de fonctionnement du système de santé nécessaires pour faire face à l’épidémie de covid-19 dans le cadre de l’état d’urgence sanitaire ](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf)
