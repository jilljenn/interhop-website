---
layout: post
title: "Seized in refere by the collective SantéNathon, the State Council recognizes that the U.S. government can access without control French health data hosted by the Health Data Hub at Microsoft." 
ref: communique-presse-le-conseil-detat-reconnait-acces-aux-donnees-de-sante
lang: en
show_comments: true
---

A group of 18 plaintiffs from the open source community, patient associations, doctors' unions, technicians and the journalism community has asked the State Council to suspend the processing and centralization of the health data of more than 67 million people in the Health Data Hub, hosted by Microsoft Azure, the American giant's Cloud.

This collective denounced Microsoft's choice essentially because of the absence of a call for tenders and the effects of the extraterritoriality of American law. Indeed, the Court of Justice of the European Union (CJEU) recently revealed that American information (via FISA and Executive Order 12,233) has no limitation on the use of European data.

<!-- more -->

Following an important [CNIL brief](https://cnll.fr/documents/35/OBSERVATIONS_DE_LA_CNIL_8_OCTOBRE_2020.pdf), and despite [an order](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000042413782) issued by the government the very day after the hearing, the State Council [recognized](https://www.conseil-etat.fr/actualites/actualites/health-data-hub-et-protection-de-donnees-personnelles-des-precautions-doivent-etre-prises-dans-l-attente-d-une-solution-perenne) that the Health Data Hub hosted by Microsoft does not protect French health data against US government intrusions, contrary to what the Ministry of Health had been claiming for months. 

Proof of the seriousness of the infringements noted, this refusal to suspend is however only a decision taken "in the very short term", notably to avoid abruptly interrupting the few projects underway on the Health Data Hub. 

On the other hand, the State Council is asking the Health Data Hub and Microsoft to include new amendments to their contracts, and to take further precautions, under the supervision of the CNIL. 

Above all, beyond the "very short term", the State Council states that it is awaiting a solution that will eliminate any risk of access to personal data by the American authorities, explicitly referring to a call for tenders from a French or European service provider, as announced by the Secretary of State for digital technologies, and as mentioned by the CNIL in its observations to the Council. 

In other words, the Health Data Hub, as it exists today, should not be able to be used in its current state beyond the few existing projects, because new projects would risk infringing the law. 

And as the CNIL demanded in its observation brief, French health data should not be hosted by Microsoft in the future, but systematically directed to one of the many existing alternatives.

**With the emergency order issued by the government and the important observations submitted by the CNIL, this is a victory that the SanteNathon collective and its members are very pleased about.**

However, given not only the lack of effective suspension but also the permanent bad faith of the Government and the Ministry of Health in this matter, the fight must continue. 

This is why, faced with the urgent need to prevent the irreversible transfer of health data to the United States, the SantéNathon collective now wishes to refer the matter to the State Council in order to take measures that go beyond the "very short term", and to the CNIL, with regard to current and past infractions. 

Other actions are also under study, notably on the European level. 

List of 18 claimants :
- **The association Le Conseil National du Logiciel Libre (CNLL)** : "*So that the discourse on digital sovereignty does not remain empty words, strategic projects on the economic level and sensitive in terms of personal freedoms should not be entrusted to operators subject to jurisdictions incompatible with these principles, but to European actors who present serious guarantees on these subjects, including through the use of open and transparent technologies.*"
- **The Ploss Auvergne-Rhône-Alpes association**.
- **The SoLibre association**
- **The company NEXEDI**: "*It is wrong to say that there was no European solution. On the other hand, it is true that the Health Data Hub never responded to the providers of these solutions.*»
- **The National Union of Journalists (SNJ)** : " *For the National Union of Journalists (SNJ), the leading organization of the profession, these actions must make it possible to maintain the secrecy of health data of French citizens and protect the secrecy of journalists' sources, the main guarantee of independent information.*"
- **The Observatory for Transparency in Medicines Policies**
- **The InterHop association**: "*The cancellation of the Privacy Shield sounds the end of European digital naivety. However, a power struggle is taking place between the United States and the European Union concerning the transfer of personal data outside of our legal space.*
*In order to ensure the sustainability of our mutual healthcare system and in view of the sensitivity of the data involved, the hosting and services of the Health Data Hub must fall under the exclusive jurisdiction of the European Union.*"
- **The Federal Union of Doctors, Engineers, Executives, Technicians (UFMICT-CGT)**
- **L'Union Générale des Ingénieurs, Cadres et Techniciens (UGICT-CGT)**: "*For the CGT des cadres et professions intermédiaires (UGICT-CGT), this recourse is essential to preserve the confidentiality of data which has now become, in all fields, a market. As designers and users of technology, we refuse to allow ourselves to be dispossessed of the debate on digital technology on the pretext that it is technical. Only democratic debate will make it possible to place technological progress at the service of human progress!*"
- **The association Constances** : "*Volunteers of Constances, the largest health cohort in France, we are particularly aware of health data and their interest for research and public health. How can it be accepted that data from French citizens is now being transferred to the United States? How can we accept that, in the long term, all the health data of the 67 million French citizens will be hosted by Microsoft and therefore fall under American laws and surveillance programs?*"
- **The French Association of Hemophiliacs (AFH)**
- **The association "Actupiennes"**.
- **The National Union of Young General Practitioners (SNJMG)** : "*Data from care must not be used for any other purpose than to improve care. Guaranteeing the security of health data and their use for public health purposes alone is a priority for all health care providers."
- **The General Medicine Union (SMG)**: "*The security of health data is a major public health issue because it allows medical confidentiality. The Health Data Hub has so far shown no guarantee that the French people's health data will be truly secure, in particular because it has chosen to host this data at Microsoft, thus jeopardizing the medical secrecy that is so necessary for a healthy and efficient therapeutic relationship."*
- **The French Union for Free Medicine (UFML)** : "*Let's avoid the control of monopolistic systems potentially harmful to the health system and to citizens".*
- **Ms. Marie Citrini, in her capacity as User Representative on the Paris Hospitals Supervisory Board**"
- **Mr Bernard Fallery, Professor Emeritus in Information Systems**: *"The 'emergency' management claimed for the Healh Data Hub is a veritable textbook case of all the risks linked to the governance of massive data: digital sovereignty and storage without a specific purpose, but also risky technical centralization, a stranglehold on a digital commons, the oligopoly of the GAFAMs, the dangers of medical secrecy, a grid of traces and adjustments to behavior.*"
- **Mr Didier Sicard, doctor and professor of medicine at the University of Paris Descartes**: *"Offering Microsoft French health data that is among the best in the world, even if it is insufficiently exploited, is a fourfold mistake: enriching Microsoft for free, betraying Europe and French citizens, preventing French companies from participating in the data analysis".*
