---
layout: post
title: "Petition to the Senate"
ref: petition-senat
lang: en
---

The Snowden case revealed to the world the massive use of our computer data through globalized surveillance programs[^snowden_europ].

In an equally brutal way, the confinement has made everyone experience the weight of deprivations of liberty imposed by a global health event.

If, according to a fundamental ethical principle that "technologies must be at the service of the individual and society" rather than "enslaved by technological giants"[^CNOM_IA], blind trust in technology carries decisive risks.

Indeed, the uncontrolled use of these new statistical tools could lead to the legitimization of anti-democratic and freedom-reducing systems.

<!-- more -->

Since November 2019, the French government has been deploying the Health Data Platform[^theconversation_Fallery] (or Health Data Hub) to develop artificial intelligence applied to health. It is a one-stop shop for access to all health data.
The data concerned are those from hospital centers, pharmacies, shared medical records and research data from various registries. The amount of data hosted is set to explode, particularly with the emergence of genomics, imaging and connected objects. It is planned that all French health data will be stored at Microsoft Azure[^HDH_microsoft], the public cloud of the American giant Microsoft. 

The citizen's refusal to abdicate this choice to use Microsoft motivates this petition. 

As citizens, we want to reaffirm our digital autonomy and create Commons for the future of our health.

The problem is that American law applies to the whole world !

Thus the CLOUDAct[^cloudact] (Clarifying Lawful Overseas Use of Data Act) allows the American justice to recover data stored on servers belonging to American companies, even if they are located in Europe[^CCBE_cloud_act]. Microsoft is subject to this text which is in conflict with our European Data Protection Regulation (DPR)[^CNIL_denis].
Worse still, with regard to the American surveillance programs, the international texts "do not in any way highlight the existence of limitations to the authorization they contain for the implementation of these programs, nor the existence of guarantees for non-American persons potentially targeted". The Court of Justice of the European Union has thus opened the breach by legally blocking the exchange of data between the European Union and the United States through the invalidation this summer of an agreement known as the "Privacy Shield".

How can we support the choice of the Microsoft company when the French President of the National Agency for Information Systems Security himself publicly opposes the digital giants that would represent an attack on our "mutualist health" systems[^ANSSI]?

How can we support this choice when the CNIL, the French supervisory authority guardian of digital freedoms, mentions in the contract binding the Health Data Platform to Microsoft "the existence of data transfers outside the European Union as part of the platform's day-to-day operation"[^CNIL_microsoft]?

How to support this choice when the CNIL specifies that the encryption keys for this data will be entrusted to Microsoft, thus making the stored data vulnerable to possible interference[^CNIL_microsoft] ?

How to support this choice when there are dozens of French and European, industrial and institutional alternatives[^HDS]?

This centralized Platform at a non-European actor is neither necessary, nor proportionate, nor adapted. 
It is a serious and surely irreversible attack on the rights of 67 million inhabitants to have their privacy protected, especially that of their most intimate data, absolutely protected by medical secrecy: their health data.

By signing this petition, you are asking the Senate to create a commission of inquiry[^proposition_com_enq] on the protection of health data. 
This commission will have to examine the conditions for signing an agreement entrusting the management of French health data to the Microsoft company. 
It will have to draw up recommendations to reinforce digital autonomy and to ensure a more secure management of health data for our health system and our fellow citizens.

The link to sign : [https://petitions.senat.fr/initiatives/i-455](https://petitions.senat.fr/initiatives/i-455)
If you're a French citizen. :-)

[^theconversation_Fallery]: [Données de santé : l’arbre StopCovid qui cache la forêt Health Data Hub](https://theconversation.com/donnees-de-sante-larbre-stopcovid-qui-cache-la-foret-health-data-hub-138852)

[^HDH_microsoft]: [Modalités de stockage du « health data hub »](http://www.senat.fr/basile/visio.do?id=qSEQ200114130&idtable=q371194%7Cq370883%7Cq370105%7Cq369641%7Cq368627%7Cq371617%7Cq371538%7Cq371754%7Cq371099%7Cq371232&_c=recherche&rch=qs&de=20170205&au=20200205&dp=3+ans&radio=dp&aff=sep&tri=dd&off=0&afd=ppr&afd=ppl&afd=pjl&afd=cvn)

[^snowden_europ]: [Trente-cinq chefs d'État étaient sous écoute de la NSA](https://www.lepoint.fr/monde/trente-cinq-chefs-d-etat-etaient-sous-ecoute-de-la-nsa-24-10-2013-1747689_24.php)

[^CNOM_IA]: [Médecins et Patients dans le monde des data, des algorithmes et de l'intelligence artificielle](https://www.conseil-national.medecin.fr/sites/default/files/external-package/edition/od6gnt/cnomdata_algorithmes_ia_0.pdf)

[^cloudact]: [Rapport Gauvain : Rétablir la souveraineté de la France et de l’Europe et protéger nos entreprises des lois et mesures à portée extraterritoriale](https://www.vie-publique.fr/sites/default/files/rapport/pdf/194000532.pdf)

[^CCBE_cloud_act]: [Évaluation du CCBE de la loi CLOUD Act des États-Unis](https://www.ccbe.eu/fileadmin/speciality_distribution/public/documents/SURVEILLANCE/SVL_Position_papers/FR_SVL_20190228_CCBE-Assessment-of-the-U-S-CLOUD-Act.pdf)


[^CNIL_denis]: [Commission spéciale Bioéthique : Auditions diverses, Mme DENIS](http://videos.assemblee-nationale.fr/video.8070927_5d6f64b41f5fe.commission-speciale-bioethique--auditions-diverses-4-septembre-2019?timecode=15077543)

[^ANSSI]: [Audition de M. Guillaume Poupard, directeur général de l'Agence nationale de la sécurité des systèmes d'information (ANSSI)](https://videos.senat.fr/video.1608918_5ebaa04d6b8cd.audition-de-m-guillaume-poupard-directeur-general-de-l-agence-nationale-de-la-securite-des-systeme?timecode=4319000)

[^CNIL_microsoft]: [Délibération n° 2020-044 du 20 avril 2020 portant avis sur un projet d'arrêté complétant l’arrêté du 23 mars 2020 prescrivant les mesures d’organisation et de fonctionnement du système de santé nécessaires pour faire face à l’épidémie de covid-19 dans le cadre de l’état d’urgence sanitaire](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf)

[^HDS]: [Annuaire des hébergeurs aggréés AFHADS](https://www.afhads.fr/wp-content/uploads/2018/05/6-Annuaire-des-membres.pdf)

[^proposition_com_enq]: [Proposition de résolution tendant à la création d'une commission d'enquête sur la protection des données de santé](https://www.senat.fr/leg/exposes-des-motifs/ppr19-576-expose.html)
