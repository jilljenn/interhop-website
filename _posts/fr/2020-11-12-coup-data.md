---
layout: post
title: "3 Questions Sur… La souveraineté numérique"
categories:
  - Data
  - Autonomie
  - HDH
redirect_from:
  - /data-capta
ref: data-capta
lang: fr
show_comments: true
---

## Quelle est votre définition de la souveraineté numérique ?

<!-- more -->

Souvent la notion de souveraineté numérique renvoie à la Nation et à sa capacité "d'agir dans le Cyberespace"[^longuet_cyber], de l'impacter et de le réguler. Parfois la définition s'inscrit plus dans un contexte géopolitique international ; c'est la capacité à "résister aux sanctions extraterritoriales"[^dsih_lemaire].

La principale limitation de ces définitions est leur prisme simplificateur.
Nous lui préférons le terme d'autonomie numérique qui questionne les enjeux d'un point de vue global. Elle se définit comme la capacité d'autodétermination dans l'environnement Cyber.
Elle peut s'entendre à plusieurs niveaux : des individus, des institutions (sécurité sociale, régions, départements, villes, hôpitaux ...), des Nations, de l'Europe, des États-Plateformes ou même de l'humanité.

Au XXIème siècle, le déséquilibre est en faveur des États Plateformes dépendants des forces du marché (Google, Apple, Facebook, Amazon, Microsoft) ou d’un régime autoritaire (Baidu, Alibaba, Tencent et Xiaomi). Actuellement l'État Plateforme est un état de fait qui remplace l'État de droit. L'applicabilité instantanée du pouvoir de l'ingénieur - son code informatique - semble même remplacer le code juridique de la Nation. Et, la formule "the Code is Law"[^lessig_code_is_law] de Lawrence Lessig, professeur de droit à Stanford, est toujours plus d'actualité.

Même si le rééquilibrage en faveur de l'État Nation doit être réaffirmé, l'autonomie numérique est à comprendre dans sa diversité et sa complexité. Elle doit inclure les multiples acteurs en présence.

[^dsih_lemaire]: [Health Data Hub : du recadrage de la CNIL à l’arrêt prévisible du service](https://www.dsih.fr/article/3925/health-data-hub-du-recadrage-de-la-cnil-a-l-arret-previsible-du-service.html)

[^longuet_cyber]: [Gérard Longuet, Franck Montaugé, « Rapport de la Commission d'enquête sur la souveraineté numérique »](http://www.senat.fr/rap/r19-007-1/r19-007-11.pdf)

[^lessig_code_is_law]: [Le code fait loi – De la liberté dans le cyberespace](https://framablog.org/2010/05/22/code-is-law-lessig/)



## Quels sont les problèmes soulevés par la gestion du cas HealthDataHub ?

Le HealthDataHub ou Plateforme des Données de Santé (PDS) vise à colliger les données du système de santé chez Microsoft et notamment sur sa plateforme Azure.

L'association interhop.org lutte aux côtés d'un collectif plus large nommé SantéNathon. Ce collectif s'oppose fermement à l'extraterritorialité du droit américain. 
Le RGPD s'applique à l'ensemble des citoyen.nes européen.ne.s vivant à l'étranger. Il possède donc aussi une composante extraterritoriale.
Cependant aux États-Unis les implications sont beaucoup plus larges.
Le droit américain autorise l'exploitation des données personnelles des non-Américains détenues par des entreprises américaines, même si l'hébergement des données est en Europe. Pire encore, les services de renseignements américains ont accès à ces donnés sans limitation ni droits opposables[^cjue_Privacy].
Interhop.org ne veut pas que les données de santé puissent être prises en otage d'intérêts géopolitiques internationaux.

De plus, un des principes fondamentaux du RGPD est la minimisation du recueil des données au regard des finalités de traitement[^minimisation]. 
Pas essence la PDS est un projet excessivement centralisateur. Elle veut centraliser l'ensemble des données de santé de plus de 67 millions de personnes. La CNIL a d'ailleurs pointé ce risque dès le lancement du projet de loi mettant en place la PDS[^minimisation_pds]. Sur ce point un débat de société est nécessaire.

En santé, le serment d'Hippocrate[^hippocrate] puis la Déclaration de Genève[^geneve] renforce la responsabilité en matière de partage des connaissances et énonce : "JE PARTAGERAI mes connaissances médicales au bénéfice du patient et pour les progrès des soins de santé".
Lorsqu'un programme est propriétaire, il n'y a aucun moyen de le tester indépendamment par des pairs : c'est une boite noire. Ceci diminue drastiquement la sécurité pour les personnes.
En fait les logiciels "libres" et "opensources" doivent même être vus comme des outils d'autonomie numérique des acteurs en présence.

Microsoft hébergeur de la PDS, ne remplit pas ces critères. Le ministère de la santé doit donc changer sa doctrine. Comme l'affirme très récemment le Conseil d'État[^cp_ce], Microsoft n'est plus une solution pérenne d'hébergement.

[^minimisation]: [Minimisation](https://www.cnil.fr/fr/definition/minimisation)

[^minimisation_pds]: [Délibération n° 2019-008 du 31 janvier 2019 portant avis sur un projet de loi relatif à l’organisation et à la transformation du système de santé (demande d’avis n° 19001144)](https://www.legifrance.gouv.fr/cnil/id/CNILTEXT000038142154/)

[^cjue_Privacy]: [La Cour invalide la décision 2016/1250 relative à l'adéquation de la protection assurée par le bouclier de protection des données UE-États-Unis](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf)

[^hippocrate]: [Serment d'Hippocrate](https://www.ordomedic.be/fr/l-ordre/serment-%28belgique%29/serment-hippocrates/)

[^geneve]: [L'Association médicale mondiale révise son serment pour les médecins](http://www.ceom-ecmo.eu/lassociation-medicale-mondiale-revise-son-serment-medecins-889)

[^cp_ce]: [Health Data Hub et protection de données personnelles : des précautions doivent être prises dans l’attente d’une solution pérenne](https://www.conseil-etat.fr/actualites/actualites/health-data-hub-et-protection-de-donnees-personnelles-des-precautions-doivent-etre-prises-dans-l-attente-d-une-solution-perenne)

## Quelles sont les alternatives ?

Historiquement la CNAM a une expertise importante en traitement des données de santé. En plus de gérer la base de données issue des remboursements de la carte vitale elle est responsable de l'appariement de plusieurs bases de données (tarification hospitalière, décès). C'est également elle qui réalise la pseudonymisation des données. Elle jouit par ailleurs des mêmes prérogatives réglementaires que la PDS[^cnam_hdh_responsable_trt]. C'est donc une alternative effective dès aujourd'hui.
Par ailleurs les hôpitaux français font depuis plusieurs années des efforts financiers et humains importants pour la création d'entrepôts de données de santé au plus proche des patients. Certains ont des infrastructures Big Data à l'état de l'art et au niveau de ce que Microsoft peut proposer. Contrairement à la PDS des dizaines de projets de recherche sont déjà actifs. 

Enfin des plateformes Big Data centralisées et éthiques existent : il y a par exemple Teralab[^teralab] (Mines Telecom) ou le Centre d'accès sécurisé aux données[^casd]. De nombreux acteurs industriels européens sont aussi en renfort et possèdent la certification "Hébergeur de données de Santé"[^hds].  Ils travaillent activement pour remplir les plus hauts niveaux de sécurité avec par exemple le label de confiance SecNumCloud de l'ANSSI.

En fait les alternatives sont légions. Alors que Microsoft représente un risque en terme d'utilisation non souhaitée des données de santé par les services de renseignements, ces acteurs sont garants des valeurs de l'Union Européenne et des libertés fondamentales. Il suffit de croire en eux. Libérons nous, nous ne voulons plus être des colonisés numériques.

[Voir l'article originel sur coupdata.fr](https://www.coupdata.fr/post/3-questions-sur-la-souverainet%C3%A9-num%C3%A9rique). 

Merci à Adrien Basdevant.

[^cnam_hdh_responsable_trt]: [Délibération n° 2020-061 du 11 juin 2020 portant avis sur un projet d'arrêté fixant la liste des organismes ou services chargés d'une mission de service public pouvant mettre en œuvre des traitements de données à caractère personnel ayant pour finalité de répondre à une alerte sanitaire, dans les conditions définies à l'article 67 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés (demande d'avis n° 20007810)](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000042071659)

[^hds]: https://esante.gouv.fr/labels-certifications/hds/liste-des-herbergeurs-certifies

[^teralab]: https://www.teralab-datascience.fr/

[^casd]: https://www.casd.eu/le-centre-dacces-securise-aux-donnees-casd/gouvernance-et-missions/

