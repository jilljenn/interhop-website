---
layout: post
title: "Le Health Data Hub n’apporte pas une information concise et aisément accessible sur les droits opposables !"
categories:
  - RGPD
  - CNIL
  - "Acces aux droits"
ref: le-healhdatahub-napporte-pas-une-information-concise
lang: fr
show_comments: true
---

Le 3 novembre dernier InterHop publiait [un appel citoyen](https://interhop.org/2020/11/03/microsoft-centralise-toujours-les-donnees-de-sante) pour lutter contre la centralisation de toutes les données de santé chez Microsoft.
> Alors que les risques inhérents au transfert de données issues du Health Data Hub vers les États-Unis est pointé par la CNIL et le Conseil d'État, la plus haute juridiction administrative française, est-ce possible de continuer d’utiliser un Microsoft Azure pour centraliser l’ensemble de nos données de santé ?

<!-- more -->


L'article proposait deux actions:
- [signer une pétition en ligne](https://petitions.senat.fr/initiatives/i-455)
- [envoyer un courriel](https://interhop.org/2020/11/03/microsoft-centralise-toujours-les-donnees-de-sante) de ```Demande d’accès à l’information concernant les données stockées et traitées au sein du Health Data Hub Microoft```

Via son président, l'Association InterHop a envoyé le courrier type ci-dessus au délégué à la protection des données du Health Data Hub HDH.

Vous retrouvez l'échange en fin d'article, de façon antéchronologique (à lire de bas en haut)


Quelques remarques : 
- La demande est lancée le 12 novembre et la première réponse utile intervient le 1 décembre
- Le dernier courriel provenant le HDH (en date du 1er décembre) est très technique et complexe. On parle du SNDS Fast Track, de SIDEP, de SIVIC... 
Comment une personne non issue du domaine de la santé numérique peut-elle comprendre les enjeux ? Où chercher ?
Le RGPD impose une **information concise, transparente, compréhensible et aisément accessible des personnes concernées**. Cette obligation de transparence est définie aux articles 12, 13 et 14 du RGPD. [La CNIL fait le point sur les mesures](https://www.cnil.fr/fr/conformite-rgpd-information-des-personnes-et-transparence){:target="blank"} permettant de respecter cette obligation.
- Le délégué à la protection des données n'arrive pas à dire si des données, en rapport avec une personne donnée, sont effectivement stockées sur le HDH. Cependant des données sont "sont effectivement hébergées sur la plateforme technologique du HDH". Il est proposé d'aider l'usager "nous vous accompagnerons pour la suite des démarches" alors la demande initiale était "j’aimerais connaître la liste exhaustive des données à caractère personnel me concernant et susceptibles d’être traitées au sein de votre plateforme." En fait il est demandé à l'usager de savoir quelles seraient les bases susceptibles de contenir des données le ou la concernant !
- L'hébergement Microsoft doit être mentionné.


<div class="alert alert-blue" markdown="1">
De: adrien.parrot <br />
A: dpo@santepubliquefrance.fr, dpo.cnam@assurance-maladie.fr, sidep-rgpd@sante.gouv.fr, dpd@health-data-hub.fr<br />
Le: mardi 1 décembre 2020 à 16:06<br />
Objet: Demande d’accès à l’information concernant les données stockées et traitées au sein du Health Data Hub Microsoft<br />


Bonjour,
 
Je pense être concerné par les bases de données OSCOUR, PMSI FastTrack, ainsi que SIVIC et STOPCovid
 
Avant de faire valoir mes droits de rectification et  d’effacement j'aimerais connaitre l'étendue des variables collectées.
Je souhaite que vous gériez vous même cette demande auprès des différents responsables de traitement.
Je ne veux pas consacrer un temps infini en m'adressant tour à tour aux différents délégués à la protection des données.
Merci par contre de me mettre en copie des échanges.
 
D'avance un grand merci<br />
Adrien PARROT
</div>

<div class="alert alert-green" markdown="1">
De: dpd@health-data-hub.fr<br />
A: dpo@santepubliquefrance.fr, dpo.cnam@assurance-maladie.fr, sidep-rgpd@sante.gouv.fr, adrien.parrot<br />
Le: mardi 1 décembre 2020 à 12:25<br />
Objet: Demande d’accès à l’information concernant les données stockées et traitées au sein du Health Data Hub Microsoft<br />

Je reviens vers vous concernant votre demande d’exercice des droits relatifs à vos données à caractère personnel.

En premier lieu, je me permets de vous présenter en quelques mots le cadre réglementaire dans lequel nous intervenons car celui-ci a une incidence directe sur la manière dont les droits reconnus par le RGPD vont s’exercer.

La loi du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé qui a créé la Plateforme des données de santé ou Health Data Hub (HDH) a aussi entendu revoir l’organisation du Système national des données de santé (SNDS). Une responsabilité conjointe du SNDS sera assurée par la Caisse nationale de l’assurance maladie et le HDH. Nous travaillons donc avec nos partenaires à la mise en place de ce système cible mais plusieurs de ses caractéristiques, en particulier les modalités d’exercice des droits, doivent encore être définies lors de la mise à jour du décret relatif au SNDS (décret n° 2016-1871 du 26 décembre 2016) qui devrait intervenir prochainement.

 

Notre objectif est que le HDH devienne un véritable outil de gouvernance et de confiance pour les citoyens et nous souhaitons mettre en place un formulaire d’exercice des droits accessible sur internet qui permettra l’expression de votre choix une fois pour toutes, en un lieu unique. Le moment venu, nous détaillerons les modalités de cette procédure mais, dans l’immédiat, notre fonctionnement est limité aux projets liés à la Covid-19 et aux projets dits « pilotes » du HDH qui ont été sélectionnés lors de deux appels à projets.

La collecte des données dans ce cadre a été adaptée par rapport à la logique envisagée dans le mode de fonctionnement pérenne du SNDS. Afin de veiller à la confidentialité des données traitées, nous fonctionnons sans identifiant de référence mais avec des identifiants spécifiques à chaque base si bien que les données d’une même personne qui se retrouveraient dans plusieurs bases de données seraient rattachées à autant d’identifiants différents qu’il y a de bases, sans aucune correspondance entre ces identifiants. Les droits relatifs aux données hébergées dans la plateforme technologique à ce jour ne peuvent donc être gérés qu’au cas par cas pour chaque base, en passant par les sources de données.

A terme, nous mettrons en place un circuit avec de (multiples) transformations du numéro de sécurité sociale (NIR), avant et après que les données arrivent sur la plateforme technologique, qui permettra d’assurer le lien avec les sources des données et facilitera l’exercice des droits des personnes pour l’ensemble des données qui les concernent.

Notre fonctionnement actuel est donc un peu plus contraignant et nous oblige à vérifier avec vous, base de données par base de données, si des données vous concernant sont ou vont se retrouver sur la plateforme technologique du HDH.

A ce jour, nous n’avons collecté que deux bases de données jugées prioritaires pour améliorer les connaissances sur l’épidémie :
- une base sur les données de l’assurance maladie depuis mars 2019 relatives aux individus avec un diagnostic hospitalier de Covid-19, appelée « SNDS Fast Track », pour laquelle les droits peuvent s’exercer par l’intermédiaire de la Caisse nationale de l’assurance maladie et de son mécanisme en vigueur pour le SNDS ;
- une base sur les résumés de passages aux urgences, dénommée « OSCOUR », communiquée par Santé publique France et qui est agrégée de telle manière que nous ne sommes pas en mesure d’identifier les personnes concernées, même en nous appuyant sur Santé publique France.

Pour plus de détails sur ces bases, je vous invite à consulter notre site internet (https://www.health-data-hub.fr/outil-de-visualisation onglet « catalogue des bases de données »).

 

Dans les prochaines semaines, nous envisageons de travailler sur les bases SI-DEP et SI-VIC qui ont été mobilisées pendant l’épidémie. SI-DEP est la base de données des résultats des tests de dépistage de la Covid-19 et SI-VIC est la base de données de suivi des victimes de situation sanitaire exceptionnelle. Ce dernier dispositif aide les autorités sanitaires à anticiper les conséquences d’un événement inattendu et grave sur l’organisation du système de santé et nous souhaitons recueillir les données relatives à la Covid-19 qu’elle contient.

 

Par ailleurs, nous allons accueillir prochainement les données nécessaires à plusieurs projets dont la réalisation sur notre plateforme technologique a été autorisée par la CNIL :
- en lien avec la Covid-19 :
  - l’étude « CoviSAS » portera sur les impacts du syndrome d’apnées obstructives du sommeil sur la survenue des  formes  graves  de la  Covid-19,
  - l’étude « Frog Covid » portera sur l’identification des facteurs prédictifs et le profil des patients à fort risque de développer une Covid-19 sévère afin de prédire les besoins et les surcoûts pharmaco-économiques de la prise en charge des patients Covid-19 hospitalisés en réanimation.

- Projets pilotes :
  - le projet « HYDRO » portera sur la prédiction des crises de décompensation cardiaque chez les patients insuffisants cardiaques porteurs de prothèse rythmique ;
  - le projet « DeepSARC » visera à identifier les meilleurs schémas thérapeutiques pour le traitement du sarcome ;
  - le projet « HUGO-SHARE » visera à analyser et prédire des événements indésirables liés aux interactions ou aux ruptures médicamenteuses ambulatoires et hospitalières dans le Grand Ouest ;
  - le projet « REXETRIS » visera à évaluer l’influence de l’immunosuppression à long terme et de son ajustement fin sur le devenir du greffon et du patient.

Afin de rester concis et compréhensible, je ne détaille pas plus ici ces projets mais je reste à votre disposition pour vous présenter ceux qui sont susceptibles de vous concerner. Nous n’avons pas encore recueilli les données correspondantes mais votre éventuelle opposition peut toujours être recueillie afin que vos données ne soient pas incluses dans le projet et donc pas transmises sur notre plateforme technologique.

D’autres projets pourront être réalisés au cours de l’année 2021 et apparaîtront au fur et à mesure dans le répertoire public que je mentionnais plus haut (onglet « répertoire des projets »).

Concernant la durée de conservation des données que vous évoquez dans votre message, celle des bases de données liées à la Covid-19 est liée à la durée de vigueur de l’article 30 de l’arrêté du 10 juillet 2020 prescrivant les mesures d'organisation et de fonctionnement du système de santé nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire. Eu égard à leur grand intérêt scientifique, même quand la crise sera terminée, nous envisageons de les conserver à plus long terme mais cela ne sera possible que si le cadre de droit commun (notamment le décret SNDS) nous le permet. Les données relatives aux projets sont quant à elles conservées pour des durées variables fixées en fonction des projets. De même, les destinataires des données sont différents selon les projets. Toutes ces informations seront également accessibles sur notre site internet dans le répertoire des projets.

Enfin, concernant le droit de rectification et le droit à l’effacement, comme expliqué au début de mon message nous nous conformerons à ce que le décret relatif au SNDS prévoira. Dans l’attente, nous pouvons y donner suite selon le circuit que je décrivais plus haut en nous appuyant sur les responsables de données, si l’un des motifs prévu à l’article 17-1° du RGPD est invoqué pour le droit à l’effacement et dans le respect des limites prévues par l’article 17-3° du RGPD relativement à l’intérêt public dans le domaine de la santé publique.

De manière générale, si vous pensez que des données à caractère personnel vous concernant sont effectivement hébergées sur la plateforme technologique du HDH ou vont l’être à court terme, alors nous vous remercions de bien vouloir nous indiquer par quelle base de données vous êtes concerné et nous vous accompagnerons pour la suite des démarches.

Je reste à votre disposition pour échanger si ces éléments n’étaient pas suffisamment clairs.

Respectueusement,

DPD, HDH
</div>


<div class="alert alert-blue" markdown="1">
De: adrien.parrot<br />
A: dpo@santepubliquefrance.fr, dpo.cnam@assurance-maladie.fr, sidep-rgpd@sante.gouv.fr, dpd@health-data-hub.fr<br />
Le: ven. 20 nov. 2020 à 09:25<br />
Objet: Demande d’accès à l’information concernant les données stockées et traitées au sein du Health Data Hub Microsoft<br />


Bonjour,
 
Merci. Avez-vous un délai ?
 
Cordialement<br />
Adrien PARROT<br />
interhop.org<br />
</div>


<div class="alert alert-green" markdown="1">
De: dpd@health-data-hub.fr<br />
A: dpo@santepubliquefrance.fr, dpo.cnam@assurance-maladie.fr, sidep-rgpd@sante.gouv.fr, adrien.parrot<br />
Le: mardi 17 novembre 2020 à 19:50<br />
Objet: Demande d’accès à l’information concernant les données stockées et traitées au sein du Health Data Hub Microsoft<br />

Bonsoir Monsieur Parrot,<br />
Nous accusons bonne réception de votre demande d'exercice de vos droits.<br />
Nous reviendrons vers vous dans les meilleurs délais pour y donner suite.<br />
Bien à vous,<br />

DPD, HDH
</div>

<div class="alert alert-blue" markdown="1">
De: adrien.parrot<br />
A: dpo@santepubliquefrance.fr, dpo.cnam@assurance-maladie.fr, sidep-rgpd@sante.gouv.fr, dpd@health-data-hub.fr<br />
Le: jeu. 12 nov. 2020 à 20:39<br />
Objet: Demande d’accès à l’information concernant les données stockées et traitées au sein du Health Data Hub Microsoft<br />

Dans le cadre des obligations telles qu’issues du Règlement Général sur la Protection des Données et de la loi Informatique et Libertés, je vous adresse cette présente demande aux fins de recevoir l’ensemble des informations concernant les données à caractère personnel qui me concernent et sont traitées au sein de la Plateforme des Données de Santé (Health Data Hub).

Conformément au 1.b de l’article 15 du RGPD, j’aimerais connaître la liste exhaustive des données à caractère personnel me concernant et susceptibles d’être traitées au sein de votre plateforme.

J’aimerais également recevoir des informations sur les délais de conservation de chacune de ces données, les destinataires de ces données.

Par ailleurs, conformément à l’article 16 du RGPD sur le droit de rectification ou encore à l’article 17 sur le droit à l’effacement pourriez-vous me confirmer dans quelle mesure les informations me concernant sont susceptibles d’être effacées ou rectifiées pour garantir l’effectivité de mes droits.

Si vous avez besoin de quelconques informations supplémentaires pour répondre à mes demandes, merci de m’en faire part par retour de mail.

Dans l’attente de votre retour, veuillez recevoir mes salutations sincères.

Adrien PARROT
</div>
