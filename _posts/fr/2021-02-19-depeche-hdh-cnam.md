---
layout: post
title: "Les Conditions ne sont pas réunies pour que Microsoft gère les données de santé, selon l'Assurance maladie"
categories:
  - HealthDataHub
  - Microsoft
  - CNAM
  - AFP
ref: depeche-hdh-cnam
lang: fr
---

## Dépêche AFP

Les "conditions juridiques nécessaires" ne "semblent pas réunies" pour confier le mégafichier des données de santé françaises "à une entreprise non soumise exclusivement au droit européen", a estimé vendredi l'Assurance maladie, désignant ainsi implicitement Microsoft.

<!-- more -->

La pilule ne passe toujours pas : saisi une nouvelle fois pour avis, sur le projet de décret devant graver dans le marbre les "modalités de mise en oeuvre" du gigantesque "système national des données de santé", le conseil d'administration de la Caisse nationale d'Assurance maladie (CNAM) ne s'est pas privé d'exprimer ses désaccords.

"Les conditions juridiques nécessaires à la protection de ces données ne semblent pas réunies pour que l'ensemble de la base principale soit mise à disposition d'une entreprise non soumise exclusivement au droit européen (...) indépendamment de garanties contractuelles qui auraient pu être apportées", écrit cette instance dans une délibération adoptée à l'unanimité des membres qui ont pris position.

La charge vise évidemment le géant américain Microsoft, choisi sans appel d'offres début 2019 pour héberger le Health Data Hub, gestionnaire désigné de ce fichier agrégeant les données de la Sécu, des hôpitaux ou des soignants libéraux, entre autres.

"Seul un dispositif souverain et uniquement soumis au RGPD (le règlement européen qui garantit aux usagers certains droits sur leurs données, ndlr) permettra de gagner la confiance des assurés", ajoute le conseil d'administration.

L'instance juge qu'en attendant cette solution, les données "ne seraient mises à disposition du Health Data Hub qu'au cas par cas", uniquement pour "des recherches nécessaires à la prévention, au traitement et à la prise en charge de la Covid-19".


## Communiqué de la présidence de la CNAM

<img src="https://i.ibb.co/j4xTdh2/Screenshot-2021-02-22-at-12-21-59.png" alt="Screenshot-2021-02-22-at-12-21-59" border="0">
<img src="https://i.ibb.co/pzHXBCG/Screenshot-2021-02-22-at-12-22-04.png" alt="Screenshot-2021-02-22-at-12-22-04" border="0">
