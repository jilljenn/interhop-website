---
layout: post
title: "[Newsletter] Victoire au Conseil d’État et financement participatif"
categories:
  - Newsletter
  - Decembre
ref: newsletter-decembre-2020
lang: fr
show_comments: true
---

Dans cette nouvelle newsletter, nous abordons 3 points : la victoire faisant suite au recours déposé auprès du Conseil d'État, à la suite l'engagement d'Olivier Véran, ministre des Solidarités et de la Santé, puis de l'importance de ton soutien via notre appel aux dons.

Pour rappel, l’association InterHop défend l’idée de Communs numériques pour la santé.

<!-- more -->


En deux mots, nous installons des logiciels décentralisés, libres et open source, et finançons des serveurs HDS qui protègent tes données de santé en respectant le RGPD ! La décentralisation est par essence protectrice des données et garante d’une recherche de qualité. 

## Mais d'abord, on se voit mercredi ?

[Rejoignez-nous sur interhop.org/webinar-dons-2020](https://interhop.org/webinar-dons-2020) **mercredi 16 décembre à 19h** pour un direct avec Karim Khelfaoui, médecin généraliste à Marseille, Juliette Alibert, juriste pour InterHop et Adrien Parrot, médecin et informaticien au sein d'InterHop. 
Tu pourras nous poser toutes les questions que tu veux concernant InterHop, nos alternatives concrètes et nos actions en justice.

## Une victoire attendue
    
Le marché "Health Data Hub" a été confié à Microsoft, entreprise américaine, sans respecter les principes du marché public : liberté d'accès à la commande publique, égalité de traitement des candidats, et transparence des procédures.

Comment peut-on avoir confiance ? Qu'en sera-t-il du traitement de nos données de santé ?

Par crainte que certaines données puissent être transférées aux États-Unis, InterHop au sein d'un large collectif de 18 requérants a saisi le Conseil d’État d’un recours demandant la suspension du Health Data Hub, géré par Microsoft.

[La juge des référés](https://interhop.org/2020/10/14/communique-presse-conseil-detat-reconnait-acces-donnes) estime qu'un risque de transfert de nos données de santé aux services de renseignements américains est bien présent.

<a href="https://zupimages.net/viewer.php?id=20/51/98il.png"><img src="https://zupimages.net/up/20/51/98il.png" alt="" /></a>

Cependant, en raison de la crise sanitaire, il refuse de suspendre le fonctionnement de la plateforme.

En revanche, elle enjoint au Health Data Hub de renforcer sur certains points le contrat qui le lie à Microsoft et de rechercher des mesures additionnelles pour mieux protéger les données qu’il héberge. Tout ceci dans l’attente d’une solution pérenne !

La [CNIL prend enfin position sur le sort du Health Data Hub](https://cnll.fr/documents/35/OBSERVATIONS_DE_LA_CNIL_8_OCTOBRE_2020.pdf) après l'invalidation du Privacy Shield (accord de transfert de donnés controversé entre les États-Unis et l’Union européenne) par le juge européen. Elle demande à l'État de repenser l'hébergement de cette base de données de santé estimant que Microsoft doit, immédiatement, être évincé. Les entreprises françaises et européennes doivent se positionner.

Des précautions devront être prises dans l’attente d’une solution pérenne qui permettra d’éliminer tout risque d’accès aux données personnelles par les autorités américaines, comme annoncé par le [secrétaire d’État au numérique Cédric O](https://www.numerama.com/tech/656229-health-data-hub-cedric-o-prevoit-de-quitter-microsoft-pour-un-prestataire-francais.html).


## Le ministre des Solidarités et de la Santé bientôt adhérent chez InterHop ? 

Monsieur le Ministre, InterHop est ouvert à votre soutien si vous partagez nos valeurs ! [Clique-ici](https://interhop.org/dons-2020).
    
Suite à la décision du juge des référés, par crainte de voir ses données de santé pillées, 
[Olivier Véran s'est engagé]() à "faire disparaître complétement" les risques que posent l'hébergement du Health Data Hub par Microsoft et à "adopter une nouvelle solution technique" d'ici deux ans, dans un courrier du 19 novembre qu'il adresse à Marie Laure Denis, la présidente de la CNIL.
    
Le ministre souscrit « pleinement » à la demande de la Commission visant à confier l’hébergement des données de santé des Français à une société soumise au droit européen, en raison de risques de transferts de celles-ci vers les États-Unis. Si cet engagement tient dans le temps, nous sommes  témoins d'une possible idylle entre InterHop et le ministre des Solidarités et de la Santé:+1:
    
<a href="https://ibb.co/bzHTB5K"><img src="https://i.ibb.co/gzvNTZR/9k4gc4n.jpg" alt="9k4gc4n" border="0"></a>

Blague à part, si l'engagement est d'héberger les données chez des entités décentralisées (!) et soumises au droit européen, nous applaudirons le Ministre. À voir...

Mais InterHop continue de râler !
Notre Ministre favori veut quitter Microsoft sous deux ans. InterHop pense que c'est beaucoup trop long, surtout avec les élections présidentielles qui arrivent...
L'idylle est-elle consommée ? Monsieur Olivier Véran, il est mensongé de dire qu'il n'existe pas de "solution optimale d'un point de vue technique" ni "d'alternative efficace et durable" à Microsoft !

InterHop le fait savoir. Nous protégeons tes droits. Nous avons écrit [une tribune dans Libération pour le dénoncer et préserver le secret médical](https://interhop.org/2020/12/14/stophealthdatahub-donnees-de-sante-en-otage-chez-microsoft), aux côtés de plus de 40 personnes physiques et morales !

## Pour nous aider à pérenniser nos travaux 

### Participe à l'appel au don lancé par InterHop

Le vendredi 27 novembre nous avons lancé notre campagne de financement participatif. Merci d'avance pour ton don, il nous permet de financer nos projets

- un documentaire qui sera prochainement diffusé

- les serveurs pour l'Hébergement de Données de Santé assurent la protection de tes données

- les [développements et l'installation de logiciels libres](https://interhop.org/projets/esante) pour la santé : messagerie instantanée chiffrée, visio-conférence sécurisée, logiciel de prise de rendez-vous décentralisé
    
- le plaidoyer : InterHop dans le cadre de ses activités de plaidoyer permet l'émergence de propositions concrètes à un niveau national et européen. Il s'agit pour InterHop d'agir en faveur d'une protection maximale des données de santé, de diffuser la culture du libre, et l'idée de décentralisation et de transparence sur les outils numériques

- maintenir nos outils d'accès aux droits. Nous voulons t'aider à appliquer le RGPD. Voici notre [FAQ dédiée](https://rgpd.interhop.org).
    
**C'est  grâce à tes dons que nous pérennisons nos travaux.**

Si tu souhaites participer, rends-toi sur notre [page dons](https://interhop.org/dons-2020/) et parle d'InterHop  autour de toi. Si tu ne peux pas participer, peut-être peux-tu relayer la campagne de don auprès de tes réseaux ?
    
C'est hyper important pour assurer notre indépendance et pouvoir t'offrir des outils libres! 
    
Quoi qu'il en soit, un grand MERCI pour ton soutien, tu nous aides énormément !

