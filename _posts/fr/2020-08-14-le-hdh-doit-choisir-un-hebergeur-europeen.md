---
layout: post
title: "Fin du Privacy Shield : le Health Data Hub doit choisir un hébergeur européen"
categories:
  - Privacy Shield
  - HDH
  - Hébergeur Européen
redirect_from:
  - /le-hdh-doit-choisir-un-hebergeur-europeen
ref: le-hdh-doit-choisir-un-hebergeur-europeen
lang: fr
show_comments: true
---

Le 31 juillet 2020 la CNIL a publié les premières informations sur les conséquences de l’invalidation du Privacy Shield. Elles sont la traduction mots pour mots de la FAQ éditée par le Comité Européen de la Protection des Données[^invalidprivacy].

Le résumé est simple : "les exigences du droit américain [...] entraînent des limitations de la protection des données personnelles qui ne sont pas circonscrites de manière à satisfaire à des exigences essentiellement équivalentes à celles requises par le droit de l'UE". En clair "**tout transfert de données vers les États Unis présente un risque**"[^usinedig].

<!-- more -->

### Revenons sur le privacy Shield

Le "Privacy Shield" (« bouclier de protection des données ») est un texte  adopté en 2016 qui permettait aux entreprises du numérique de transférer légalement les données personnelles (identité, comportement en ligne, géolocalisation…) de citoyens européens aux Etats-Unis.

Ce texte a été attaqué par "M.  Maximillian  Schrems  utilisateur de Facebook  depuis  2008.  Comme  pour  les  autres  utilisateurs  résidant  dans  l’Union,  les données  à caractère  personnel  de  M.  Schrems  sont,  en  tout  ou  en  partie,  transférées par  Facebook  Ireland vers des serveurs appartenant à Facebook Inc., situés sur le territoire des États-Unis, où elles font l’objet d’un traitement. M. Schrems a déposé une plainte auprès de l’autorité irlandaise de contrôle, visant, en substance, à faire interdire ces transferts. Il a soutenu que le droit et les pratiques des États-Unis  n’offrent  pas  de  protection  suffisante  contre l’accès,  par  les  autorités  publiques, aux données  transférées  vers  ce  pays."[^CJUE_PrivacyShield]

Voici la réponse de la Cour de Justice de l'Union Européenne ou CJUE. "Les limitations de la protection des données à caractère personnel qui découlent de la réglementation interne des États-Unis portant sur l’accès et l’utilisation, par les autorités publiques américaines, de telles données transférées depuis l’Union [Européenne] vers ce pays tiers ne sont pas encadrées d’une manière à répondre à des exigences équivalentes à celles requises, en droit de l’Union, par le principe de proportionnalité, en ce que les programmes de surveillance fondés sur cette réglementation ne sont pas limités au strict nécessaire. En se fondant sur les constatations figurant dans cette décision, la CJUE relève que, **pour certains programmes de surveillance, la dite réglementation ne fait ressortir d’aucune manière l’existence de limitations à l’habilitation qu’elle comporte pour la mise en œuvre de ces programmes, pas plus que l’existence de garanties pour des personnes non américaines potentiellement visées**."[^CJUE_PrivacyShield]

Les programmes de surveillance Etats-Uniens n'ont pas de limite quant à l'utilisation des données. La CJUE invalide donc l’accord global que constituait le Privacy Shield. 

Par contre elle confirme que les entreprises peuvent se conformer à la loi européenne en s’engageant, individuellement, à respecter certaines précautions quant à l’usage des données de leurs utilisateurs européens, via les Clauses Contractuelles Types CCT. Dans ce cas il faut  un "niveau de protection équivalent à celui garanti au sein de l'Union Européenne par le RGPD". Dans le cas contraire les transferts doivent être suspendu ou interdit[^invalidprivacy].

Mr Schrems s'est félicité de cette décision en annonçant "un changement fondamental qui va bien plus loin que les transferts de données entre UE et Etats-Unis. Les autorités comme la commission de protection des données irlandaise ont sapé le succès du RGPD jusque-là. La cour dit clairement qu’elles doivent se mettre au travail et faire respecter la  loi"[^CJUE_lemonde].

Suite à cet arrêt, la CNIL Allemande souligne que “les données ne devraient pas être transférées aux États-Unis tant que ce cadre juridique n’aura pas été réformé”[^regulateurallemand].


### Et pour nos données de santé ?

Le Health Data Hub ou “Plateforme des Données de Santé” regroupe l’ensemble des données de santé de plus de 67 millions de personnes chez Microsoft Azure, le cloud du géant américain.
Ces données constituent les plus sensibles des données à caractère personnel.

La CNIL[^CNILdel] a aussi précisé qu'eu égard à la sensibilité des données en cause, elle souhaiterait que l'hébergement et les services liés à la gestion du Health Data Hub puissent être réservés à des entités relevant exclusivement des juridictions de l'Union européenne. 
L'Agence nationale de la sécurité des systèmes d'information (ANSSI), par la voix de son directeur général Guillaume POUPARD[^poupard], a aussi précisé qu'il serait préférable de privilégier une solution européenne non soumise à des lois extra-européennes.
Récemment le Conseil d'État[^conseil_etat] met en avant que les données de la Plateforme pourraient transiter par les États-Unis lorsqu'elles seront traitées.

Lors de l’audience au Conseil d’État le 11 juin, nous avons appris que, même si Microsoft pouvait garantir la localisation du stockage des données, ce n’était pas le cas pour les données d’analyse. Ces dernières, une fois copiées sur les processeurs de Microsoft, circulent dans le monde entier. Nous ne parlons ici pas des données de maintenance qui ne sont pas des données de santé.

Le 2 juillet dernier le Contrôleur Européen de la Protection des Données a émis un rapport[^CEPD_produits_microsoft] sur les produits Microsoft. Le manque d’information et de contrôle sur les données d'analyse a été clairement pointé et était "préoccupant". Il a aussi été pointé que "**les institutions de l’UE n’ont pas été en mesure de contrôler l’emplacement d’une grande partie des données traitées par Microsoft**" et que "**Microsoft divulgue des données personnelles** (y compris les données sur les clients, les données sur les administrateurs, les données de paiement et les données d’assistance) à des tiers, y compris les services répressifs ou d’autres agences gouvernementales."

Il est maintenant  primordial de faire appel à des fournisseurs qui ne sont pas soumis à l'extraterritorialité du droit américain : Patriot Act et CLOUD Act. Les CCT ne sont en aucun cas suffisantes pour empêcher un accès aux données par les autorités Américaines surtout dans le cadre de programmes de surveillance américains.

Nous affirmons donc que l'hébergement de la Plateforme des Données de Santé, pour être conforme à la décision de la CJUE doit être confié à un opérateur européen. La CJUE requiert des exigences équivalentes à celles du droit de l'Union Européenne. À ce stade, "tout transfert vers les États-Unis comporte un risque" et "les transferts effectués sur la base de ce cadre juridique sont illégaux".

#### Sources

[^CJUE_PrivacyShield]: [La Cour invalide la décision 2016/1250 relative à l'adéquation de la protection assurée par le bouclier de protection des données UE-États-Unis](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf)

[^CEPD_produits_microsoft]: [Résultats de l’enquête par les institutions européennes concernant l’utilisation des produits et services Microsoft](https://pad.interhop.org/itdQNkLlT8GEG08yUIspVQ#)

[^invalidprivacy]: [Invalidation du « Privacy shield » : les premières questions-réponses du CEPD](https://www.cnil.fr/fr/invalidation-du-privacy-shield-les-premieres-questions-reponses-du-cepd)

[^linkedun]: https://www.linkedin.com/feed/update/urn:li:activity:6695272380106870784/

[^usinedig]: [L'annulation du Privacy Shield doit s'appliquer immédiatement, tranche la Cnil européenne ](https://www.usine-digitale.fr/article/l-annulation-du-privacy-shield-doit-s-appliquer-immediatement-tranche-la-cnil-europeenne.N989069)

[^CJUE_lemonde]: [L’accord sur le transfert de données personnelles entre l’UE et les Etats-Unis annulé par la justice européenne](https://www.lemonde.fr/pixels/article/2020/07/16/la-justice-europeenne-annule-l-accord-sur-le-transfert-de-donnees-personnelles-ue-etats-unis_6046344_4408996.html)

[^regulateurallemand]: [https://www.dataguidance.com/news/berlin-berlin-commissioner-issues-statement-schrems-ii-case-asks-controllers-stop-data](https://www.dataguidance.com/news/berlin-berlin-commissioner-issues-statement-schrems-ii-case-asks-controllers-stop-data)

[^poupard]: [Office parlementaire d'évaluation des choix scientifiques et technologiques](https://videos.senat.fr/video.1608918_5ebaa04d6b8cd.audition-de-m-guillaume-poupard-directeur-general-de-l-agence-nationale-de-la-securite-des-systeme?timecode=4319000)

[^conseil_etat]: [Conseil d'État, 19 juin 2020, Plateforme Health Data Hub](https://www.conseil-etat.fr/ressources/decisions-contentieuses/dernieres-decisions-importantes/conseil-d-etat-19-juin-2020-plateforme-health-data-hub)

[^CNILdel]: [Mesures d’organisation et de fonctionnement du système de santé nécessaires pour faire face à l’épidémie de covid-19 dans le cadre de l’état d’urgence sanitaire ](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf)
