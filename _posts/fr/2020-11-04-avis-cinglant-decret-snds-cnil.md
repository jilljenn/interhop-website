---
layout: post
title: "Délibération de la CNIL portant avis sur un projet de décret relatif au SNDS"
categories:
  - HDH
  - Microsoft
  - CNIL
  - RGPD
redirect_from:
  - /avis-cinglant-decret-snds-cnil 
show_comments: true
lang: fr
ref: avis-cinglant-decret-snds-cnil 
---

[Next Inpact](https://www.nextinpact.com/article/44419/health-data-hub-sombre-diagnostic-dr-cnil) publie aujourd'hui le projet d'avis de la CNIL concernant le décret d'application de la [loi de juillet 2019](https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006072665/LEGISCTA000031923877/2020-03-19/) portant sur le Système National des Données de Santé SNDS.

Le décret rendrait possible, en "droit commun" le traitement des données de Santé Publique par la Plateforme des données de Santé PDS (ou le Health Data Hub). Il donnera une base légale, inexistante avant ledit décret, seule la loi du 24 juillet 2019 ayant été promulguée.

Ce décret devait paraitre en octobre 2020. Il a été fortement critiqué par la CNIL et n'est toujours pas paru début décembre 2020.

<!-- more -->

Voici le [projet de décret](https://cdn2.nextinpact.com/medias/projet-de-decret-snds.pdf) et l'[avis de la CNIL](https://cdn2.nextinpact.com/medias/avis-cnil-snds.pdf).

Un résumé ci après des points les plus importants.

# Remarques générales

Le rapport de la CNIL est remarquable, cinglant et accablant pour l’Etat :

- Manque de clarté : périmètres, contenus et réglementations applicables aux différents SNDS "SNDS historique", "SNDS élargi" et "SNDS centralisé"

- Imprécisions nombreuses dans le cadre de l'articulation et la mise en oeuvre des droits d'opposition ou encore d'information, mais également quant aux listes de données concernées et aux différentes modalités applicables selon les bases. Cela rend le projet illisible et impraticable à l'exercice des droits par les citoyen.ne.s

- Irrégularités majeures : détention du Numéro d'inscription au répertoire NIR (Numéro de sécurité sociale) par la PDS rendant la ré-identification possible au sein de la PDS (alors que toute l'architecture technique repose sur le stockage de données strictement pseudonymisées chez Microsoft), impossibilité pour la PDS d'avoir comme prérogative de vérifier la conformité des bases au RGPD (catalogue de données) compétence relevant normalement de la CNIL.

- Flou dans la répartition des rôles de la CNAM et la PDS

Dans un tel contexte, l'entrée en vigueur d'un tel décret semble compromise, sauf à revoir entièrement la copie. La meilleure façon de reprendre sereinement le sujet serait d'abandonner l'option Microsoft, retenir des opérateurs souverains (Cloud et software), le tout dans une solution décentralisée avec une répartition dans la chaine des acteurs, leurs rôles et les données concernées qui soit lisible pour les citoyen.ne.s.

# Analyse points par points

- Point 5 : exigence de la CNIL d'une interdiction de transfert des données de santé vers les US qui concernerait l'ensemble des données du SNDS (et non dans le seul cadre des données traitées pendant l'Etat d'Urgence Sanitaire EUS) mais ne règle que partiellement le problème au regard des effets extraterritoriaux du droit US - auquel est soumis Microsoft - et pratiques omnipotentes des services de renseignement.

### Sur le périmètre du SNDS

- les points 6 et 7: contestation indirecte du choix d'une solution centralisée alors qu'une solution décentralisée était totalement jouable (et déjà effective). Un SNDS "élargi" n'implique pas forcément de centraliser les données (et par conséquent des transferts de données), ce qui accroît les risques en cas de piratage.

- les points 26, 27, 28: Le ministère est son propre organe de contrôle ! La CNIL devrait contrôler le respect du RGPD et donner un avis préalable sur toute introduction de nouvelle bases pour s'assurer de la proportionnalité des données hébergées par rapport aux finalités fixées par la loi.
S'agissant de l'information relative à la constitution du SNDS

- point 32 : le projet de décret ne prévoit pas d'information individuelle des personnes alors c'est une obligation réglementaire. La CNIL rappelle l'exigence d'information. Une campagne d'information par voie postale est requise pour environ 30 % de la population, ne disposant pas de compte Ameli !

### S'agissant des modalités d'exercice des droits des personnes

- point 37 : modalités flous compte-tenu du grand nombre d'acteurs, équivalent à une absence de possibilité d'exercer effectivement ses droits d'opposition.

- point 39. Inapplicabilité du droit d’opposition pour la base principale et pour la base catalogue

- point 40 : La CNIL considère que l'opposition de la réutilisation des données d'une base source devrait empêcher la remontée des données dans le catalogue; elle estime également nécessaire de prévoir un droit d'effacement des données.

- point 41 : rappelle l’obligation d’une portée rétroactive du droit d’opposition – y compris pour les données dans le cadre de l’EUS.

- points 44 - 52 : pour la PDS au sein du SNDS centralisé, il est formellement interdit de détenir le NIR, sinon on peut revenir à l'individu. C'est à un tiers de confiance de détenir le NIR. Le tiers de confiance détient la clef de passage entre le NIR et l'identifiant crypté. Pas l'hébergeur.

- points 45- 46 : Concernant le droit d'accès et de rectification, la solution consistant à s'adresser soit à la CNAM soit aux organismes responsables de chaque base de données alimentant le SNDS ne permet pas un exercice effectif des droits. Entre les "différents" SNDS, les bases du catalogue etc., il sera quasiment impossible de mettre en oeuvre ce droit.

### Sur les responsabilités respectives de la CNAM, de la PDS et des différents acteurs

- Points 55 et 56: CNAM et PDS responsables conjoints. Illisibilité des rôles de chacun. A préciser.

### Sur l'accès permanent de certains services publics au SNDS

- Point 64 - 66 : liste des accès permanents a été largement augmentée malgré l'incomplétude des fiches transmises pour demander cet accès permanent. La CNIL relève que la plupart des organismes n’ont pas fait de demandes d'accès ou encore que ces accès permanents ne sont pas justifiés.

- point 72 : concernant les accès permanents, la personne n'a pas de droit d'opposition valable
Autres points

- les points 82 et 83: Rappel de la CNIL - les données recueillies dans le cadre de la crise doivent être détruites après la fin de la crise sanitaire. Problème quant à la réalité pratique de cette exigence : travail titanesque si les données ont été appariées avec d'autres données.

- les points 85 et 86: comme indiqué précédemment, la CNIL semble contre la centralisation, compte-tenu du risque de piratage facilité par cette centralisation, et autres problèmes de sécurité au regard du choix du prestataire.
Concernant la duplication des données, la CNIL relève qu'il existera donc de multiples copies des données. Par exemple le PMSI sera dans les hôpitaux, à l'ATIH, à la CNAM et à la PDS.

# Autres points susceptibles d'être soulevés / critiqués et non relevés par la CNIL

- La CNIL ne se prononce pas sur un des aspects du projet de décret contenu au 3) de l'article 1er. En effet, l'appariemment de nombreuses bases de données du catalogue avec la base principale est facilité. Cette réalité prouve qu'il est facile d'identifier les jeux de données dès lors qu'elles peuvent être liées ensemble au sein du HDH mais proviennent de bases différentes.

- La CNIL ne se prononce pas sur l'élargissement de la nature des données collectées et traitées. Nous notons le flous des données environnementales et socio-économiques.
> "Informations relatives aux conditions sociales, environnementales, aux habitudes de vie et au contexte socio-économique des personnes concernées" 
