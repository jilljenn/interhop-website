---
layout: post
title: "#StopHealthDataHub : les données de santé en otage chez Microsoft"
categories:
  - Presse
  - Tribune
  - HealthDataHub
  - Microsoft
  - CJUE
  - Alternatives
ref: stophealthdatahub-donnees-de-sante-en-otage-chez-microsoft
lang: fr
show_comments: true
---

> Contestable en matière de sécurité, le projet de centralisation des données de santé des Français est inconciliable avec le respect des droits à la protection des informations personnelles, alertent des associations, personnalités publiques, syndicats des secteurs de la santé et de la défense des libertés.

<!-- more -->

[Voir l’article dans Libération](https://www.liberation.fr/debats/2020/12/14/stophealthdatahub-les-donnees-de-sante-en-otage-chez-microsoft_1808434){:target="_blank"}

<a style="text-align: center;" class="button alt" type="blue" href="https://forms.interhop.org/node/23" target="_blank">Signer la tribune !</a>

Voté fin mars 2019 dans le cadre de la loi santé et faisant suite aux annonces présidentielles, il a été décidé de mettre en oeuvre un Big Brother médical : le Health Data Hub.

Cette plateforme visant à centraliser l'ensemble des données de santé de plus de 67 millions de personnes veut faire avancer la recherche en santé.

Déjà contestable au regard des risques en matière de sécurité, cette centralisation de l'ensemble des données de santé de la population française est inconciliable avec le respect des droits à la protection des données personnelles.

En effet, l'hébergement des données du Health Data Hub repose sur le géant du numérique Microsoft, une société soumise au droit américain.

Dans un courrier du 19 novembre dernier, le Ministre de la Santé lui-même a reconnu que cette solution n'était pas viable[^courrier_veran].

Et pour cause ! Petit rappel des faits.

Dans une affaire récente portée devant la Cour de Justice de l’Union Européenne contre Facebook[^cjue], le juge a reconnu que le droit américain ne protégeait pas les données personnelles des européens. Risque d'accès massif aux données par les services de renseignement américain, absence d'autorité indépendante pour contrôler ces accès, défaut de droits opposables pour les citoyens, cette décision rend illicite les transferts de données vers les Etats-Unis. Suite à cette décison, Facebook a même fait pression sur l’Union Européenne en menaçant d'arrêter ses services d’ici la fin de l’année[^facebook].

Les implications de cette décision pour les données de santé hébergées sur des clouds américains, comme le Health Data Hub, sont colossales !

Pour rappel, les données de santé constituent les informations les plus sensibles et intimes, protégées par le secret médical.

Dans ce cadre, plusieurs associations et organisations voulaient obtenir l'arrêt immédiat du stockage et du traitement des données de plus de 67 millions de personnes au sein du Health Data Hub.

Le 13 octobre 2020, après plusieurs recours en urgence et un avis historique de la CNIL[^avis_cnil] considérant cette solution comme néfaste, le Conseil d’État [^conseil_etat] a considéré que le Health Data Hub ne protégeait pas les données de santé contre les intrusions du gouvernement américain. De façon critiquable, il a cependant admis que le projet pouvait prospérer dans le cadre de la lutte contre la covid19.

En réaction, le 19 novembre 2020, le Ministre de la Santé[^courrier_veran] a lui-aussi reconnu ces risques tout en promettant le retrait de Microsoft mais dans un délai de deux ans !

DEUX ANS.

Le temps pour les données, pourtant couvertes par le secret médical, d'être bradées ou accaparées par des intérêts commerciaux ou politiques.

DEUX ANS.

Le temps pour des accords transnationaux de permettre une régularisation des transferts des données vers les Etats-Unis, alors que les droits opposables sont d'ores et déjà ineffectifs au Health Data Hub.

DEUX ANS.

Le temps d'oublier les enjeux de sécurité autour des données de santé et de perdre la confiance des patients. 

DEUX ANS

Le temps que la réversibilité de la solution soit techniquement impossible ou financièrement trop coûteuse. La réversibilité en informatique désigne la possibilité, pour un client ayant sous-traité son exploitation à un infogérant, de récupérer ses données à l'issue d'un contrat. 

DEUX ANS.

Le temps de faire croire que face à l'urgence liée à la covid19, d'autres alternatives à Microsoft n'étaient pas possibles.

Cela serait un affront à la recherche médicale que de penser qu'elle a attendu le Health Data Hub pour lutter contre la Covid19.

Des plateformes techniques efficaces existent.

Les acteurs institutionnels de la santé développent déjà des plateformes d'analyse sécurisées répondant aux besoins des chercheurs. En leur sein, des centaines de projets de recherche sont en cours, y compris sur la covid19. Ils développent des approches décentralisatrices qui garantissent localement le lien de confiance avec les patients. 
D'autres comme le “Centre d’Accès Sécurisé aux Données” conçoivent des solutions matérielles (hardware) pour renforcer la sécurité de bout en bout.
Ces plateformes, pour la plupart centrées sur le logiciel libre et loin des intérêts extra-européens, sont protectrices des données hébergées. Leur développement est la 3ème voie européenne pour l’autonomie numérique de la prochaine décennie.

Nous sommes pour le développement de la recherche, et pour garder la confiance des patients, faisons confiance en notre capacité à inventer et à créer. 

DEUX ANS.

Le temps pourtant opportun et nécessaire pour repenser la gouvernance du projet du Health Data Hub avec la société civile. La gouvernance doit être séparée des problématiques d'hébergement des données. Le Health Data Hub doit se limiter à sa mission historique : être un organe de régulation de l’accès aux données.

Pour l'ensemble de ces raisons, nous demandons une refonte structuelle du projet Health Data Hub ainsi que le retrait IMMEDIAT de Microsoft Azure.

<a style="text-align: center;" class="button alt" type="blue" href="https://forms.interhop.org/node/23" target="_blank">Signer la tribune !</a>

Signataires : 

InterHop
- Syndicat de la Médecin Générale (SMG)
- Syndicat National des Jeunes Médecins Généralistes (SNJMG)
- Union Française pour une Médecine Libre (UFML)
- Fédération des Médecins de France (FMF)
- InterSyndicale Nationale des Internes (ISNI)
- Association Constances
- Actions Traitements
- Les Actupiennes
- Collectif Bas les masques
- Observatoire de la Transparence dans les politiques du médicament (OTMeds)
- Fédération SUD Santé Sociaux
- Union fédérale Médecins, ingénieurs, cadres, techniciens (UFMICT) CGT
- Syndicat National des Journalistes (SNJ)
- Maison des Lanceurs d'Alerte
- Nothing2Hide
- Sciences Citoyennes
- April, promouvoir et défendre le logiciel libre
- La Quadrature du Net (LQDN)
- Le Mouton Numérique
- Le Mouvement
- Marie Citrini, Représentante des Usagers de l'AP-HP
- Didier Sicard, Professeur émérite de médecine à l'Université Paris Descartes, ancien président du Comité consultatif national d'éthique (CCNE)
- Israël Nisand, Professeur émérite, Université de Strasbourg
- Jean-Paul Hamon, Président d'honneur de la Fédération des Médecins de France (FMF)
- Anne Gervais, Hépatologue, membre du Collectif Inter-Hôpitaux
- Sophie Crozier, Neurologue, membre du Comité Consultatif National d'Ethique (CCNE), membre du Collectif Inter-Hôpitaux
- Karim Khelfaoui, Médecin Généraliste
- Franck Ehooman, Anesthésiste-Réanimateur
- Michel Heisert, Gynécologue-Obstétricien
- Laurent Mauduit, journaliste, co-fondateur de Médiapart
- Bernard Fallery, Professeur émérite, Université de Montpellier
- Antoine Deltour, Lanceur d'alerte Luxleaks
- Christiane Féral-Schuhl, Présidente du Conseil National des Barreaux (CNB)
- Sophie Ferry Bouillon, Membre du Conseil National des Barreaux (CNB)
- Nathalie Martial-Braz, Professeur de droit privé, Université Paris Descartes
- Sabrina Calvo, autrice, Collectif Zanzibar
- Stuart Calvo, éditrice, Collectif Zanzibar
- Alain Damasio, auteur, Collectif Zanzibar

<a style="text-align: center;" class="button alt" type="blue" href="https://forms.interhop.org/node/23" target="_blank">Signer la tribune !</a>

[^courrier_veran]: https://www.documentcloud.org/documents/7331959-Courrier-Veran.html#document/p2

[^cjue]: https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf

[^facebook]: https://www.lebigdata.fr/facebook-interrogation-activite-europe

[^conseil_etat]: https://www.conseil-etat.fr/actualites/actualites/health-data-hub-et-protection-de-donnees-personnelles-des-precautions-doivent-etre-prises-dans-l-attente-d-une-solution-perenne

[^avis_cnil]: https://www.documentcloud.org/documents/7224049-Me-moireCnilHDH.html#document/p1
