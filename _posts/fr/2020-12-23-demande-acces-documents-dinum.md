---
layout: post
title: "Demande de documents administratifs : audit de sécurité du Health Data Hub"
categories:
  - Courrier
  - Audit
  - DiNum
  - Health Data Hub
ref: demande-acces-doc-adm-audit-secu-dinum
lang: fr
---

<p style="text-align:right;">
Nadi BOU HANNA <br/>
Directeur interministériel du numérique <br/>
20 Avenue de Ségur, 75007 PARIS
</p>


Paris, le 23 décembre 2020

<!-- more -->

Monsieur,

Dans le cadre des débats qui ont lieu depuis mars dernier au sujet du Health Data Hub hébergé chez Microsoft Azure, la directrice du Groupement Intérêt Public (GIP) "Health Data Hub" mentionne tout récemment un audit de sécurité réalisé par la Direction Interministériel du Numérique (DINUM)[^1]. 

Le Health Data Hub vise à développer l’intelligence artificielle appliquée à la santé et veut devenir un guichet unique d’accès à l’ensemble des données de santé.
Les données concernées sont celles des centres hospitaliers, des pharmacies, du dossier médical partagé et les données de recherche issues de divers registres. La quantité des données hébergées est amenée à exploser, notamment avec l’émergence de la génomique, de l’imagerie et des objets connectés. Ces données sont stockées chez Microsoft Azure, cloud public du géant américain Microsoft.

Afin d’éclairer nos positions et de pouvoir jouer son rôle d’expertise et de vigie de la démocratie,  l'association InterHop demande de communiquer le document administratif suivant, en application des articles L311-1 à R311-15 relatifs du Code des Relations entre le Public et l'Administration à la communication des documents administratifs [^2]  : 
- avis public de la DINUM concernant l'audit de sécurité du Health Data Hub.

Nous souhaitons recevoir ces documents :
- dans un format numérique, ouvert et réutilisable. 
- dans un format papier

Nous souhaitons également, dans l'intérêt des citoyen.ne.s à avoir accès aux informations administratives, que ce document soit publié comme le mentionnait la Directrice du Health Data sur le site de la DINUM, notamment dans la rubrique dédiée aux publications : [https://www.numerique.gouv.fr/publications/](https://www.numerique.gouv.fr/publications/){:target="_blank"}

Nous vous remercions de nous indiquer, après publication, leur adresse de téléchargement et nous les envoyer en pièce jointe.

Comme le livre III du code des relations entre le public et l’administration le prévoit lorsque le demandeur a mal identifié celui qui est susceptible de répondre à sa requête, je vous prie de bien vouloir transmettre cette demande au service qui détient les documents demandés si tel est le cas.

Veuillez agréer, Madame, Monsieur, l'expression de nos sentiments distingués.

Adrien PARROT 
Président InterHop

[^1]: [https://www.youtube.com/watch?v=rB9xvOSRLgw&feature=youtu.be](https://www.youtube.com/watch?v=rB9xvOSRLgw&feature=youtu.be){:target="_blank"}

[^2]: [https://www.cada.fr/particulier/mes-droits](https://www.cada.fr/particulier/mes-droits){:target="_blank"}
