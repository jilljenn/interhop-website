---
layout: post
title: "Webinar campagne de dons 2020"
categories:
  - Dons
  - Webinar
ref: webinar-dons-2020
lang: fr
redirect_from:
  - /webinar-dons-2020
show_comments: true
---

L’association InterHop défend l’idée de Communs numériques pour la santé.

Tu pourras nous poser toutes les questions que tu veux concernant InterHop, nos alternatives concrètes et nos actions en justice.

<img src="https://zupimages.net/up/20/51/emnm.jpeg" alt="" />

<!-- more -->

Rejoins-nous **mercredi 16 décembre à 19h** pour un direct avec 
- Karim Khelfaoui, médecin généraliste à Marseille
- Juliette Alibert, juriste pour InterHop
- Adrien Parrot, médecin et informaticien au sein d'InterHop 

<a style="text-align: center;" class="button alt" type="blue" target="_blank" href="https://peertube.interhop.org/videos/watch/41f20825-e44d-4a28-aeb3-385a97cfe450">Revoir le Direct!</a>

# InterHop

En deux mots, nous installons des logiciels décentralisés, libres et open source, et finançons des serveurs HDS qui protègent tes données de santé en respectant le RGPD ! La décentralisation est par essence protectrice des données et garante d’une recherche de qualité. 

# Au programme : nos projets et notre appel à dons  Pour nous aider à pérenniser nos travaux 

Le vendredi 27 novembre nous avons lancé notre campagne de financement participatif. Merci d'avance pour ton don, il nous permet de financer nos projets

- un documentaire qui sera prochainement diffusé

- les serveurs pour l'Hébergement de Données de Santé assurent la protection de tes données

- les [développements et l'installation de logiciels libres](https://interhop.org/projets/esante) pour la santé : messagerie instantanée chiffrée, visio-conférence sécurisée, logiciel de prise de rendez-vous décentralisé
    
- le plaidoyer : InterHop dans le cadre de ses activités de plaidoyer permet l'émergence de propositions concrètes à un niveau national et européen. Il s'agit pour InterHop d'agir en faveur d'une protection maximale des données de santé, de diffuser la culture du libre, et l'idée de décentralisation et de transparence sur les outils numériques

- maintenir nos outils d'accès aux droits. Nous voulons t'aider à appliquer le RGPD. Voici notre [FAQ dédiée](https://rgpd.interhop.org).
    
**C'est  grâce à tes dons que nous pérennisons nos travaux.**

Si tu souhaites participer, rends-toi sur notre [page dons](https://interhop.org/dons-2020/) et parle d'InterHop  autour de toi. Si tu ne peux pas participer, peut-être peux-tu relayer la campagne de don auprès de tes réseaux ?
    
C'est hyper important pour assurer notre indépendance et pouvoir t'offrir des outils libres! 

A mercredi !
