---
layout: post
title: "Le gouvernement contraint les hôpitaux à abandonner vos données chez Microsoft"
categories:
  - Tribune
  - HDH
  - Microsoft
  - CloudAct
  - RGPD
  - Arrêté
redirect_from:
  - /le-gouvernement-contraint-les-hopitaux-a-abandonner-vos-donnees-chez-microsoft/
show_comments: true
lang: fr
ref : gouv_order
---

[Signer ce texte](https://forms.interhop.org/node/16)

Un [arrêté](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000041812986&dateTexte=20200421) publié le 21 avril oblige les hôpitaux à intensifier l'envoi de vos données chez Microsoft.

Contrairement à l'avis de nombreux acteurs - Commission National Informatique et Libertés, Ordre National des Médecins, Conseil National des Barreaux, hôpitaux - le gouvernement français s’appuie sur le géant américain Microsoft pour stocker l'ensemble des données de santé. Nous appelons à la constitution d’un écosystème universitaire, médiatique, juridique, associatif et politique pour réaffirmer les valeurs d'autonomie et des "[communs](https://framablog.org/2020/04/04/pour-un-plan-national-pour-la-culture-ouverte-leducation-ouverte-et-la-sante-ouverte/)" et, pour faire naître un large débat de société.

<!-- more -->

<div class="alert alert-red" markdown="1">
## Microsoft récolte vos données de santé
La plate-forme nationale des données de santé ou [Health Data Hub](https://solidarites-sante.gouv.fr/actualites/presse/communiques-de-presse/article/communique-de-presse-agnes-buzyn-health-data-hub-officiellement-cree-lundi-2) (HDH) est en cours de déploiement. Le HDH est un guichet unique d’accès à l’ensemble des données de santé pour développer l’intelligence artificielle appliquée à la santé.
Ces données sont celles de tous les citoyens français et concernent l'ensemble des systèmes informatisés : hôpitaux, pharmacies, dossiers médicaux partagés et données de recherche issues de divers registres... La quantité de données hébergées est amenée à exploser, notamment avec l’émergence de la génomique, de l’imagerie et des objets connectés. Toutes ces données sont stockées chez [Microsoft Azure](https://www.lesechos.fr/idees-debats/cercle/opinion-soignons-nos-donnees-de-sante-1143640), plateforme cloud du géant américain Microsoft.
En présentant un projet de recherche d'["intérêt public"](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038821260&categorieLien=id) - concept juridiquement flou - les GAFAM (Google, Apple, Facebook, Amazon et Microsoft), les start-up ainsi que les [assureurs](https://www.mediapart.fr/journal/france/221119/health-data-hub-le-mega-fichier-qui-veut-rentabiliser-nos-donnees-de-sante?onglet=full) pourront désormais accéder aux données de santé et au pouvoir financier qu’elles représentent. 

Cette privatisation de la santé est perçue comme dangereuse pour de nombreux acteurs : 
- un député de LREM Pierre-Alain Raphan est le premier à dénoncer l'infrastucture logicielle Microsoft dans [LesEchos](https://www.lesechos.fr/idees-debats/cercle/opinion-soignons-nos-donnees-de-sante-1143640)
- Martin Hirsch, directeur des hôpitaux parisiens, s'inquiète du risque de "compromettre la confiance des patients" dans [Mediapart](https://www.mediapart.fr/journal/france/221119/health-data-hub-le-mega-fichier-qui-veut-rentabiliser-nos-donnees-de-sante) 
- un [collectif](https://pad.interhop.org/s/Sk7K8qpaS#) initié par des professionnels de la santé et de l’informatique médicale s'inquiète dans une tribune au [Monde]({{ site.baseurl }}/donnees-de-sante-au-service-des-patients/)
- le [Conseil National des Barreaux](https://www.cnb.avocat.fr/sites/default/files/11.cnb-mo2020-01-11_ldh_health_data_hubfinal-p.pdf) met en garde contre "les risques de violation du secret médical et d’atteinte au droit au respect de la vie privée"
- un [collectif d'entreprises de logiciels d’édition](https://www.santenathon.org/) dénonce "le non-respect des principes d’égalité et de transparence dans le choix de Microsoft Azure"
- le [Conseil National de l'Ordre des Médecins](https://www.conseil-national.medecin.fr/sites/default/files/external-package/edition/od6gnt/cnomdata_algorithmes_ia_0.pdf) alerte sur le fait que "les infrastructures de données, plateformes de collecte et d’exploitation, constituent un enjeu majeur sur les plans scientifique, économique, et en matière de cyber-sécurité. La localisation de ces infrastructures et plateformes, leur fonctionnement, leurs finalités, leur régulation représentent un enjeu majeur de souveraineté afin que, demain, la France et l’Europe ne soient pas vassalisées par des géants supranationaux du numérique"
- récemment la [Commission National Informatique et Libertés](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf) (CNIL) rappelle qu'à cause de la "sensibilité des données en cause" il faut que le stockage des données "puisse  être réservé  à  des  entités relevant exclusivement des juridictions de l’Union européenne".
</div>

<div class="alert alert-green" markdown="1">
## Les hôpitaux ont résisté
Les Français applaudissent tous les soirs à 20h pour remercier et encourager le personnel soignant, mais également pour réaffirmer les valeurs d'entraides et de solidarité. 

La main tendue par le [secteur privé et l'arrivée de nombreux volontaires](https://www.aphp.fr/contenu/ap-hp-deja-plus-de-16-000-volontaires-non-soignants-ont-propose-leur-aide-pendant-la-crise) au sein des hôpitaux lors de la crise du coronavirus démontrent qu'il est nécessaire d´apporter des appuis technologiques et humains pour aider le secteur public. Cependant le secteur public doit être le garant de nos droits sur nos données. Ainsi, les hôpitaux de Paris ont refusé la [proposition de Palantir](https://www.bfmtv.com/tech/donnees-de-sante-l-ap-hp-ecarte-la-proposition-de-palantir-1894772.html) - entreprise soumise au CloudAct et [travaillant pour la NSA, le FBI et la CIA](https://www.france24.com/fr/20200424-covid-19-les-espions-sortent-de-l-ombre-pour-lutter-contre-la-pand%C3%A9mie?ref=li) -  de participer à l'élaboration "des outils numériques de suivi de l'épidémie de Covid-19". 

Cependant, un [arrêté](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000041812986&dateTexte=20200421) publié le 21 avril oblige les hôpitaux à intensifier l'envoi de vos données au HDH-Microsoft, témoignant d'une [nouvelle contradiction de fond](https://lvsl.fr/le-health-data-hub-ou-le-risque-dune-sante-marchandisee/), "entre la logique du soin inconditionnel propre au secteur public ainsi qu’au serment d’Hippocrate, et les exigences d’efficacité et de rentabilité aujourd’hui dénoncées par les personnels médicaux et hospitaliers à travers de leur mouvement de grève et de leur réaction à la crise du Covid-19".

</div>

<div class="alert alert-red" markdown="1">
## Le CloudAct ou la rupture du secret médical ?
Le gouvernement américain a adopté en 2018 un texte nommé [Cloud Act](https://www.vie-publique.fr/sites/default/files/rapport/pdf/194000532.pdf) qui permet [à la justice américaine](https://experiences.microsoft.fr/business/confiance-numerique-business/faut-il-avoir-peur-du-cloud-act/) d’avoir accès aux données stockées dans des pays tiers. La présidente de la Commission Nationale de l’Informatique et des Libertés (CNIL) a affirmé, en septembre, à l'[Assemblée Nationale](http://videos.assemblee-nationale.fr/video.8070927_5d6f64b41f5fe.commission-speciale-bioethique--auditions-diverses-4-septembre-2019?timecode=15077543) que ce texte est contraire au [Règlement Général sur la Protection des Données](http://www.privacy-regulation.eu/fr/48.htm) (RGPD) censé protéger les citoyens européens. 

En cas de volonté politique ou d'attaque informatique,  les patients sont soumis à un risque de rupture du secret médical.
Quel serait l’impact d’une fuite massive de données de santé ? 
</div>

<div class="alert alert-green" markdown="1">
## Il existe des alternatives technologiques
Nous sommes favorables à l'utilisation encadrée d'outils de l'intelligence artificielle en santé. Cependant, des alternatives aux GAFAM qui mettent en avant le respect de la vie privée et le secret médical existent. Elles garantissent l’indépendance face aux enjeux géo-politiques et commerciaux supra-étatiques  ainsi que le contrôle collectif des infrastructures.

Les hôpitaux créent localement des entrepôts de données de santé pour collecter et analyser les données générées in situ. Grâce à cette décentralisation, l’échange de données entre les régions et nos voisins [européens](https://www.ehden.eu/) est possible tout en préservant la sécurité des données. 
Les centres hospitaliers produisent et collectent des données. En mettant localement en contact soignants et chercheurs de tout domaine (dont l'intelligence artificielle), leurs expertises favorisent le développement des nouvelles technologies et renforcent l’interconnexion entre le soin et la recherche.

Des acteurs français du cloud existent : [OVH](https://www.ovhcloud.com/fr/enterprise/solutions/certified-cloud-solutions/healthcare-data-hosting-hds/) par exemple. La volonté de créer un ["cloud européen"](https://www.lemondeinformatique.fr/actualites/lire-ovh-outscale-le-cloud-souverain-vraiment-ressuscite-76657.html) avec le renfort potentielle de la direction interministérielle du numérique et du système d'information et de communication de l'État (DINSIC) est une opportunité d'unir les efforts pour construire la médecine de demain, libre, interopérable, sécurisée et indépendante.
</div>


<div class="alert alert-orange" markdown="1">
## Appel : le coronavirus créera un débat de société
Les données de santé sont à la fois un bien d’usage des patients et le patrimoine inaliénable de la collectivité. Il est essentiel de garder le contrôle sur les technologies déployées (algorithmes transparents,  infrasctructures autonomes), et d’empêcher la privatisation de la santé.
Au moment où les risques de [surveillance de masse sont toujours plus d'actualité](https://www.arte.tv/en/videos/083310-000-A/7-billion-suspects/) et où le gouvernement veut utiliser [les données personnelles pour lutter contre le coronavirus](https://cnnumerique.fr/files/uploads/2020/2020.04.23_COVID19_CNNUM.pdf), il est venu le temps "[d’établir notre autonomie numérique](https://www.lemonde.fr/idees/article/2020/04/25/tracage-numerique-le-moment-est-venu-d-etablir-notre-souverainete-numerique_6037729_3232.html)". 
Nous appelons à une information et une mobilisation citoyenne large. Nous souhaitons la constitution d'un écosystème universitaire, médiatique, juridique, associatif et politique autour de ces questions pour faire naître un large débat de société.
</div>

[Signer ce texte](https://forms.interhop.org/node/16)
