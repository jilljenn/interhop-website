---
layout: post
title: "La fin de Microsoft au sein du Health Data Hub est acquise"
categories:
  - Data
  - HDH
  - RGPD
  - Microsoft
ref: fin-de-microsoft-veran
lang: fr
show_comments: true
---


InterHop se félicite de l'avis du Ministre de la Santé Mr Olivier Véran qui donne raison aux actions du collectif SantéNathon. 

<!-- more -->

Le Ministre de la santé reconnait les risques de l'hébergement chez Microsoft du Health Data Hub. 

Cependant InterHop dénonce le délai de réversibilité trop important au regard de la sensibilité des données hébergées par le Health Data Hub.

Retrouver [l'échange complet](https://www.documentcloud.org/documents/7331959-Courrier-Veran.html#document/p2){:target="_blank"} entre Le Ministre de la Santé et La Présidente de la CNIL Mme Marie-Laure DENIS.

Voici le [communiqué de presse](https://interhop.org/2020/10/14/communique-presse-conseil-detat-reconnait-acces-donnes){:target="_blank"} de SantéNathon après l'Ordonnance du Conseil d'Etat.

Voici la liste des 18 requérants :
- **L’association Le Conseil National du Logiciel Libre ([CNLL](https://cnll.fr/))** : « *Pour que les discours sur la souveraineté numérique ne restent pas des paroles en l'air, les projets stratégiques au plan économique et sensibles au plan des libertés personnelles ne doivent pas être confiés à des opérateurs soumis à des juridictions incompatibles avec ces principes, mais aux acteurs européens qui présentent des garanties sérieuses sur ces sujets, notamment par l'utilisation de technologies ouvertes et transparentes.* »
- **L’association Ploss Auvergne-Rhône-Alpes**, membre du CNLL
- **L’association SoLibre**, membre du CNLL
- **La société NEXEDI** : « *Il est faux de dire qu'il n'y avait pas de solution européenne. Il est exact en revanche que le Health Data Hub n'a jamais répondu aux offreurs de ces solutions.* »
- **Le Syndicat National des Journalistes (SNJ)** : « *Pour le Syndicat national des journalistes (SNJ), première organisation de la profession, ces actions doivent permettre de conserver le secret sur les données de santé des citoyennes et citoyens de France ainsi que protéger le secret des sources des journalistes, principale garantie d’une information indépendante.* »
- **L'Observatoire de la transparence dans les politiques du médicament**
- **L'association InterHop**  : « *L'annulation du Privacy Shield sonne la fin de la naiveté numérique européenne. Cependant des rapports de force se mettent en place entre les Etats-Unis et l'Union Européenne concernant le transfert  des données personnelles en dehors de notre espace juridique. Pour pérenniser notre système de santé mutualiste et eu égard à la sensibilité des données en cause, l'hébergement et les services du Health Data Hub doivent relever exclusivement des juridictions de l’Union européenne.* »
- **L'Union Fédérale Médecins, Ingénieurs, Cadres, Techniciens (UFMICT-CGT)**
- **L'Union Générale des Ingénieurs, Cadres et Techniciens (UGICT-CGT)**: « *Pour la CGT des cadres et professions intermédiaires (UGICT-CGT), ce recours est indispensable pour préserver la confidentialité des données qui sont désormais devenues, dans tous les domaines, un marché. Concepteurs et utilisateurs des technologies, nous refusons de nous laisser déposséder du débat sur le numérique au prétexte qu'il serait technique. Seul le débat démocratique permettra de placer le progrès technologique au service du progrès humain!* »
- **L'association Constances** : « *Volontaires de Constances, la plus grande cohorte de santé en France, nous sommes particulièrement sensibilisés aux données de santé et leurs intérêts pour la recherche et la santé publique. Comment admettre que des données de citoyens français soient aujourd'hui transférées aux Etats-Unis ? Comment accepter qu'à terme, toutes les données de santé des 67 millions de Français soient hébergées chez Microsoft et donc tombent sous les lois et les programmes de surveillance américains ?* »
- **L'association Française des Hémophiles (AFH)**
- **L'association les "Actupiennes"**
- **Le Syndicat National des Jeunes Médecins Généralistes (SNJMG)** : « *Les données issues des soins ne doivent pas servir d'autre finalité que l'amélioration des soins. Garantir la sécurité des données de santé et leur exploitation à des seules fins de santé publique est une priorité pour toustes les soignant.es.* »
- **Le Syndicat de la Médecine Générale (SMG)**: « *La sécurisation des données de santé est un enjeu majeur de santé publique puisqu'elle permet le secret médical. Le Health Data Hub n'a jusqu'ici montré aucune garantie sur une véritable sécurisation des données de santé des Français.es, notamment par son choix d'héberger celles-ci chez Microsoft, et met ainsi en danger le secret médical pourtant nécessaire à une relation thérapeutique saine et efficiente*. »
- **L’Union Française pour une Médecine Libre (UFML)** : « *Évitons le contrôle de systèmes monopolistiques potentiellement nuisibles pour le système de santé et les citoyens.* »
- **Madame Marie Citrini, en son mandat de représentante des usagers du Conseil de surveillance de l'AP-HP**
- **Monsieur Bernard Fallery, professeur émérite en systèmes d’information** : « *La gestion "par l’urgence" revendiquée pour le Healh Data Hub est un véritable cas d’école de tous les risques liés à la gouvernance des données massives : souveraineté numérique et stockage sans finalité précisée, mais aussi centralisation technique risquée, mainmise sur un commun numérique, oligopole des GAFAM, dangers sur le secret médical, quadrillage des traces et ajustement des comportements* »
- **Monsieur Didier Sicard, médecin et professeur de médecine à l’Université Paris Descartes** : « *Offrir à Microsoft les données de santé françaises qui sont parmi les meilleures du monde, même si elles sont insuffisamment exploitées, est une quadruple faute : enrichir gratuitement Microsoft, trahir l'Europe et les citoyens français, empêcher les entreprises françaises de participer à l'analyse des données* »

