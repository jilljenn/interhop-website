---
name: "Soignant.e.s"
subtitle: "Nos logiciels E-Santé"
order: 1
image_path: /images/projets/heart.png
show_image: false
type: main
projets_page: true
ref: esante
lang: fr
---

<div class="container">
	<ul class="image-grid">
		{% assign _projets = site.projets | where: "lang", "fr" | where: "type", "esante" | sort: 'order' %}
		{% for projet in _projets %}
			<li>
			<!--	<a href="{{ site.baseurl }}{{ projet.url }}"> -->
					<img src="{% include relative-src.html src=projet.image_path %}" alt="{{ person.name }}">
					<div class="details">
						<div class="name">{{ projet.name }}</div>
						<div class="position">{{ projet.subtitle }}</div>
					</div>
			<!--	</a> -->
			</li>
		{% endfor %}
		<li class="filler"></li>
	</ul>
</div>

<div class="alert alert-green" markdown="1">
### Jitsi ou la vidéo-conférence
Jitsi permet de faire des visioconférences 100 % open source et sécurisées. 
Le service bénéficie d’une haute qualité audio et vidéo qui permet de faciliter vos échanges à distance entre professionnels de santé ou entre soignant.e.s et soigné.e.s.

Cette solution ne nécessite pas de création de compte. L’outil est accessible sur PC et Mac mais également sur mobile et tablette. 

Lorsque vous créez un salon de discussion sur Jitsi, un lien est alors généré. Ce lien est une clé unique et il vous suffit de le copier à vos participants.

### Messagerie instantanée avec Element !

Element est une application de chat sécurisée pour les professionnels de santé. 

Elle vous permet de garder le contrôle de vos conversations, à l'abri de l'exploitation de données et des publicités. 

Les échanges réalisés via Element  sont chiffrés de bout en bout.

### Cryptpad : un espace de stockage chiffré et sécurisé 

CryptPad est une alternative aux outils Microsoft et aux services cloud propriétaires. Ce logiciel est respectueuse de la vie privée.

Tout le contenu stocké dans CryptPad est chiffré avant d'être envoyé, ce qui signifie que personne ne peut accéder à vos données à moins que vous ne leur donniez les clés (même pas nous !).

### Goupile : formulaires de recherche en santé
[Goupile](https://goupile.fr/){:target="_blank"} est un logiciel de création de formulaires.
Il est parfait pour les études scientifiques notamment en santé.

Il permet de réaliser des Cahiers d'observation électronique (ou eCRF).

Pour ton projet, InterHop peut ouvrir des accès à Goupile et développer de nouvelles fonctionnalités adaptées à tes besoins spécifiques.

Tu es chercheur.se ? N'hésite pas à nous contacter par courriel ou [via notre formulaire de contact]({{site.baseurl}}/nous).


</div>

<div class="alert alert-red" markdown="1">
### Serveurs "Hébergeur de Données de Santé" HDS

Pour faire fonctionner ces logiciels nous avons besoin d'ordinateurs. Ces ordinateurs ou serveurs doivent remplir des conditions de sécurité spécifique pour la Santé. Interhop travaille avec un [Hébergeur de Données de Santé certifié](https://esante.gouv.fr/labels-certifications/hebergement-des-donnees-de-sante){:target="_blank"} : [GPLExpert](https://gplexpert.com/hebergement-donnees-sante-hds/){:target="_blank"}.

L'ensemble des données sont hébergées en France. Notre partenaire, GPLExpert dépend strictement du droit français et européen. Son siège social est en France, à Arpajon.

Nous travaillons activement pour délivrer ces services dans les conditions de sécurité nécessaires pour les données de santé.

Nos outils E-Santé ne sont pas encore disponibles. Ils le seront pour la santé **dès le premier trimestre 2021**.

</div>

Abonne-toi à notre lettre d'information pour être tenu au courant.
<p><a style="text-align: center;" class="button alt" type="blue" href="{{ site.baseurl }}/nous/">Lettre d'Information</a></p>

<div class="alert alert-red" markdown="1">
### Budget
900 euros / mois.

Nous louons déjà les serveurs HDS et tu peux déjà nous aider en donnant. :-)
L’argent servira d'abord à financer des serveurs dédiés à la santé ou HDS.
</div>

Aide-nous en faisant un don pour InterHop.
<p><a style="text-align: center;" class="button alt" type="blue" href="{{ site.baseurl }}/dons/">Dons !</a></p>


