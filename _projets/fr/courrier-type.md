---
name: Accès aux Droits
subtitle: Courrier Type
order: 2
image_path: /images/projets/courrier-type.png
type: acces-aux-droits
ref: courrier-type
lang: fr
redirect_to:
    - https://interhop.org/2020/11/03/microsoft-centralise-toujours-les-donnees-de-sante
---
