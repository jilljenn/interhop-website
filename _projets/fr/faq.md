---
name: Accès aux Droits
subtitle: Foire aux Questions
order: 1
image_path: /images/projets/faq.png
show_image: true
type: acces-aux-droits
ref: faq
lang: fr
---

C’est pour répondre aux questions que tu te poses sur le RGPD que j’ai voulu créer cette FAQ.
J’ai d’ailleurs beaucoup appris en la construisant.

**Tu as accès à notre FAQ 24h/24 et 7j/7**. Elle est libre. 

La FAQ, je l’ai voulu **dynamique**, c'est-à-dire que tu entreras ta question, tes mots clés, pour arriver sur la page sur laquelle tu trouveras la réponse.

N’hésite pas à me poser, par mail (dpd@interhop.org), les questions auxquelles tu n’auras pas trouver de réponse dans la FAQ. Tu m’aideras à l'enrichir.

Tu peux me tirer les oreilles si je suis trop technique à ton goût, ça me permettra de me réajuster.

En fait, mon but c’est de te rendre autonome dans la mise en œuvre de bonnes pratiques concernant le RGPD.

<p><a style="text-align: center;" class="button alt" type="blue" target="_blank" href="https://rgpd.interhop.org">Voici la FAQ de InterHop</a></p>

* **Notre FAQ est libre**, ça veut dire tu peux y avoir accès sans don. Cependant, je dois t'avouer que tu nous aides en participant par le biais d'un don (même petit). 

* **Tu peux aussi vouloir faire une veille, poser une question pour t'aider à comprendre et à appliquer** le RGPD dans ta structure, bref! Allez plus loin. Alors InterHop met à ta disposition les ressources nécessaires (juriste, ingénieurs, informaticiens, DPO…). Si besoin, nous sollicitons l’autorité de contrôle pour t'aider et poser la bonne question. Dans tous les cas, tu obtiens une réponse écrite entre 24 et 72 heures. 
On estime notre travail de recherche à environ 50 euros par question (recherche, échange en équipe, lien avec toi...).

* **Si tu veux qu'on t'aide à appliquer**, on remplira ensemble un cahier des charges pour répondre réellement à ta demande et te satisfaire. On établira un devis en adéquation avec la mission que tu nous confies.

Dans tous les cas, j’espère que notre FAQ fera ton bonheur :)

À bientôt,

Chantal, DPO
