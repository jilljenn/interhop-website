---
name: Nos Collaborations
subtitle: Développeurs, collaborons !
order: 3
image_path: /images/projets/collaboration.png
show_image: true
type: main
ref: collaboration
lang: fr
---

# Voici les projets en lien avec InterHop.

### [Goupile](https://goupile.fr){:target="_blank"}
<div class="alert alert-blue" markdown="1">
- **But:** Formulaire de recueil de données
- **Techno:** Lit-html, c++, Postgresql
- **Contacts:** contact@goupile.fr
- **Site Internet:** [goupile.fr](https://goupile.fr){:target="_blank"}
</div>

Goupile est un logiciel de création de formulaires permettant la création de formulaires complexes.

Il s'efforce de rendre la création de formulaires et la saisie de données à la fois puissantes et faciles.
Il est parfait pour les études scientifiques notamment en santé.

Il est actuellement en cours de développement et il n'existe pas encore de version publique.

### [Toobib](https://toobib.org){:target="_blank"}
<div class="alert alert-blue" markdown="1">
- **But:** Application opensource de prise de rendez-vous en santé (phase de test)
- **Gitlab:** https://framagit.org/toobib/toobib-website
- **Techno:** React, Flask, Postgres
- **Contacts:** 
  - interhop@riseup.net
  - contact@toobib.org
- **Site Internet:** [toobib.org](https://toobib.org){:target="_blank"} en construction
</div>

Toobib.org est une application web permettant la prise de rendez-vous entre professionnels de santé et soignés.

Elle se veut protectrice des données de personnes puisqu’elle ne stocke qu’un courriel associé à un horaire de prise de rendez-vous.

Les futurs développement de Toobib.org veulent promouvoir la décentralisation et la féderation d’instances. Toobib.org utilise aussi les standards d’interopérabilité en santé comme FHIR.

### Transmed
<div class="alert alert-blue" markdown="1">
- **But:** Application opensource de transmission médicale (phase de test)
- **Techno:** React, Django, Postgres
- **Contact:** 
  - interhop@riseup.net
</div>

On vous en dit plus bientôt.

### [Susana](https://susana.interhop.org){:target="_blank"}
<div class="alert alert-blue" markdown="1">
- **But:** Application web d'alignement des terminologies médicales
- **Gitlab:** https://framagit.org/interchu/conceptual-mapping
- **Techno:** openUI, Flask, Postgres
- **Contact:** 
  - interhop@riseup.net
</div>

Susana est une application web permettant l'alignement terminologique entre des terminologies sources (ex : celles des hôpitaux) et des terminologies standards définies par les équipes de l'OHDSI (Observational Health Data Sciences and Informatics).
Il s'agit d'un site web collaboratif, international, ergonomique et basé sur l'utilisateur.
Il combine
- un outil de recherche en texte libre
- un outil de cartographie basé sur l'utilisateur

En tant qu'outil de collaboration, Susana permet la convergence de la terminologie entre les bases de données et les langues.

### Mise en forme et exploitation des fichiers décès INSEE
<div class="alert alert-blue" markdown="1">
- **But:** Mise en forme et exploitation des fichiers décès INSEE
- **Gitlab:** https://gitlab.com/antoinelamer/fichiers_deces_insee/
- **Techno:** R, Python
- **Contact:** 
  - interhop@riseup.net
</div>

L'INSEE rend public les fichiers des personnes décédés depuis 1970 : [https://insee.fr/fr/information/4190491](https://insee.fr/fr/information/4190491){:target="_blank"}.

Les fichiers sont mis à jour mensuellement et contiennent les informations suivantes : nom, prénoms, sexe, date de naissance, code et libellé du lieu de naissance, date du décès, code du lieu de décès et numéro de l’acte de décès.

Les scripts python et R de ce répertoire proposent les fonctionnalités suivantes :
- compiler et nettoyer les fichiers annuels et mensuels en un fichier unique pour faciliter les opérations de record linkage avec d'autres sources de données
- rechercher des correspondances dans la base INSEE à partir d'un fichier local
- visualiser les enregistrements (nombre de décès hebdomadaires)

### Transformation du SNDS au format OMOP
<div class="alert alert-blue" markdown="1">
- **But:** Partage d'informations sur le mapping structurel du [SNDS](https://www.snds.gouv.fr/SNDS/Accueil) vers [OMOP](https://www.ohdsi.org/data-standardization/the-common-data-model/)
- **Gitlab:** https://framagit.org/interchu/snds-structural-mapping
- **Techno:** Python
- **Contact:** 
  - interhop@riseup.net
</div>

Les différents organismes participant au projet sont invités à déposer à un format facilement exploitable humainement leurs propositions de mapping dans un folder au nom de leur organisme.

### Pseudonymisation des Comptes-Rendus medicaux
<div class="alert alert-blue" markdown="1">
- **But:** Pipeline de pseudonymisation des comptes-rendus.
- **Gitlab:** https://framagit.org/interchu/omop-note-deid
- **Techno:** Spark, Python
- **Contact:** 
  - interhop@riseup.net
</div>

Extraction d'entité, assignation fictive, génération de comptes-rendus déidentifiés.

