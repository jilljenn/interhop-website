---
name: "Citizens"
subtitle: Access to rights
order: 4
external_name: RGPD
external_url: https://www.cnil.fr/fr/reglement-europeen-protection-donnees
show_image: false
type: main
projets_page: true
image_path: /images/projets/droit.png
ref: acces-aux-droits
lang: en
---

InterHop facilitates access to people's rights.

<div class="container">
	<ul class="image-grid">
		{% assign _projets = site.projets | where: "lang", "en" | where: "type", "acces-aux-droits" | sort: 'order' %}
		{% for projet in _projets %}
			<li>
				<a href="{{ site.baseurl }}{{ projet.url }}"> 
					<img src="{% include relative-src.html src=projet.image_path %}" alt="{{ person.name }}">
					<div class="details">
						<div class="name">{{ projet.name }}</div>
						<div class="position">{{ projet.subtitle }}</div>
					</div>
				</a> 
			</li>
		{% endfor %}
		<li class="filler"></li>
	</ul>
</div>

### Frequently Asked Questions
We want to make you autonomous in the implementation of good practices regarding the General Regulation on Data Protection or RGPD.
This is the text that protects the rights of European citizens.

Here is our [FAQ](https://rgpd.interhop.org){:target="_blank"}.

### Mail Type

To facilitate the administrative and regulatory procedures, we pre-edit standard letters to make the link with our institutions.

The CNIL already proposes [letters to act](https://www.cnil.fr/fr/modeles/courrier){:target="_blank"}.

We will complete the offer :-)
Here is a [blog post]({{site.baseurl}}/2020/11/03/microsoft-centralise-toujours-les-donnees-de-sante) on the subject and the Health Data Hub.

### Techno-legal watch

You can make a watch thanks to InterHop :-)

InterHop relays relevant information in the field of digital health, European and international law.

You can find them on our [Zotero account]({{site.baseurl}}/zotero) or here [interhop.org/press](https://interhop.org/press), translated when they are broadcast in English.
