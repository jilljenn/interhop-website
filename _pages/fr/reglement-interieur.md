---
layout: presentation
title: Réglement intérieur
permalink: reglement-interieur/
ref: reglement-interieur
lang: fr
---


Adopté par l’assemblée générale du 30/06/2020

# Article 1 – Agrément des nouveaux membres.
Tout nouveau membre doit être parrainé et présenté par un membre de l’association.
Il est agréé par le conseil statuant à la majorité de tous ses membres.
Le conseil statue lors de chacune de ses réunions sur les demandes d’admission présentées.
Les personnes désirant adhérer doivent remplir un bulletin d’adhésion.

# Article 2 – Démission – Exclusion – Décès d’un membre
1. La démission doit être adressée au président du conseil par lettre recommandée. Elle n’a pas à être
motivée par le membre démissionnaire.
2. Comme indiqué à l’article 7 des statuts, l’exclusion d’un membre peut être prononcée par le conseil, pour
motif grave. Sont notamment réputés constituer des motifs graves :
- la non-participation aux activités de l’association ;
- une condamnation pénale pour crime et délit ;
- toute action de nature à porter préjudice, directement ou indirectement, aux activités de l’association ou à
sa réputation.
En tout état de cause, l’intéressé doit être mis en mesure de présenter sa défense, préalablement à la
décision d’exclusion.
La décision d’exclusion est adoptée par le conseil statuant à la majorité des deux tiers des membres
présents.
3. En cas de décès d’un membre, les héritiers ou les légataires ne peuvent prétendre à un quelconque
maintien dans l’association.
La cotisation versée à l’association est définitivement acquise, même cas en cas de démission, d’exclusion,
ou de décès d’un membre en cours d’année.

# Article 3 – Assemblées générales – Modalités applicables aux votes
1. Votes des membres présents
Les membres présents votent à main levée. Toutefois, un scrutin secret peut être demandé par le conseil

# Article 4 – Indemnités de remboursement.

Aucun membre ni administrateur ne peut prétendre au remboursement des fais engagés dans le cadre de
leurs fonctions et sur justifications.

# Article 5 – Commission de travail.

Des commissions de travail peuvent être constituées par décision du conseil d’administration.

# Article 6 – Modification du règlement intérieur

Le présent règlement intérieur pourra être modifié par le conseil ou par l’assemblée générale ordinaire à la
majorité simple des membres.
