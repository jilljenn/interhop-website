---
layout: presentation
title: Statuts
permalink: statuts/
ref: statuts
lang: fr
---

<p style="text-align:center;">
INTERHOP<br/>
ASSOCIATION REGIE PAR LA LOI DU 1er JUILLET 1901<br/>
SIEGE : 142 rue du Faubourg Saint Martin, 75010 PARIS
</p>


<br/>
<br/>
<br/>

# PREAMBULE

InterHop.org promeut et développe l'utilisation des logiciels libres et open-sources pour la santé. Elle entend oeuvrer pour des communs en santé et déployer une utilisation des données de santé éthique et transparente, respectueuse du droit à la protection des données personnelles dont les données couvertes par le secret médical sont une composante particulièrement sensible.

InterHop.org regroupe des militants du logiciel libre.

Elle entend favoriser une approche auto-gérée des données de santé à l'échelle locale. 

# ARTICLE 1 – DENOMINATION
Il est fondé, entre les adhérents appelés membres fondateurs aux présents statuts, une association régie par la loi du 1er juillet 1901 et le décret du 16 août 1901, ayant pour dénomination : InterHop.


# ARTICLE 2 – OBJET
Cette association a pour objet :
- la promotion de l'interopérabilité des systèmes d'information et du logiciel libre en santé, dont la sensibilisation, la formation, ou le déploiement d'initiatives d'éducation populaire en lien avec les activités de l'association,
- l'hébergement de données de santé générées par l'exploitation de logiciels libres,
- la fédération d'acteurs mobilisés sur les enjeux liés au libre, à l'interopérablité, à la décentralisation et à la protection des garanties associées à la préservation de la vie privée,
- la promotion et le déploiement d'actions juridiques et d'accès au droit, de communication, de plaidoyer et campagnes en faveur de la protection des données de santé et des libertés fondamentales qui y sont associées,
- le renforcement et la contribution au développement d'alternatives locales, décentralisées, libres, transparentes et éthiques,
- la protection des productions numériques en santé,
- et plus généralement l’exercice de toute activité liée directement et/ou indirectement à l’objet ci-dessus.

# ARTICLE 3 - SIEGE
Le siège de l’association est fixé 142 rue du Faubourg Saint Martin, 75010 PARIS. Il pourra être transféré par simple décision du Conseil d’Administration.

# Article 4 – DUREE 
La durée de l’association est illimitée.

# ARTICLE 5 – COMPOSITION ET ADMISSION DES MEMBRES
L’association se compose de membres, personnes physiques et morales, se répartissant en deux catégories, à savoir :

1. Membres fondateurs de l'association

2. Membres adhérents :

Les membres adhérents sont admis conformément au règlement intérieur.

- Membres personnes morales : l'adhésion de personnes morales doit être approuvée par la majorité des 2/3 du Conseil d'Administration et nécessite le versement d'une cotisation annuelle dont le montant est fixé par le Conseil d'Administration.
Elles sont représentées par leurs représentants légaux ou les personnées déléguées à cet effet.
Ils ont voix délibérative lors de l'Assemblée Générale.

- Membres personnes physiques : il s'agit des personnes physiques s'étant acquittées du montant de la cotisation annuelle fixée par le Conseil d'Administration.
Ils ont voix délibérative lors de l'Assemblée Générale.

# ARTICLE 6 - RADIATIONS et SUSPENSIONS
La qualité de membre se perd par :
1. La démission notifiée au Conseil d'Administration ;
2. Le décès ;
3. La radiation prononcée par le Conseil d'Administration pour non-versement répété de la cotisation annuelle, ou pour manquement grave aux valeurs de l'association, pour non-respect de ses statuts ou de son règlement intérieur, ou pour comportement incompatible avec le fonctionnement de l'association. L'intéressé peut demander dans un délai de 15 jours à compter de de la notification de la décision par le Conseil d'Administration, présenter des explications orales devant le bureau et/ou par écrit. 

Pour les mêmes motifs que pour la radiation, la qualité de membre peut être suspendue temporairement sur décision du Conseil d'Administration notamment dès lors qu'est envisagée une procédure de radiation, conformément au point 3 de cet article.

# ARTICLE 7 – AFFILIATION 
L’association peut adhérer à d’autres associations, unions ou regroupements par décision du Conseil d’Administration. Par ailleurs, d’autres associations, unions ou regroupements pourront adhérer à l’association dans les conditions présentées par l'article 5.

# ARTICLE 8 – RESSOURCES
Les ressources de l’association sont constituées pas:
- les cotisations des membres, 
- les financements participatifs (crowdfunding) ou produits de collectes, 
- les dons,
- les subventions autorisées par la loi (subventions publiques, subventions de fondations oeuvrant dans l'intérêt général)
- les recettes provenant de la vente de publications, créations ou prestations de services fournis conformément à son objet
- les legs
- toutes ressources autres autorisées par la loi ou le règlement

Au-delà d'un certain montant ou lorsqu'il s'agit de dons d'entreprises, ceux-ci pourront être refusés par le Conseil d'Administration.

# ARTICLE 9 - MODALITES DE TENUE A DISTANCE DES INSTANCES ET MODALITES DE VOTES

La tenue des différentes instances de l'association (Assemblées générales et extraordinaires, Conseil d'Administration, Bureau) peut être réalisée à distance par voie de visioconférence et /ou conférence téléphonique.

Les votes des personnes assistant à distance aux instances sont comptabilisés de la même manière que les votes des personnes en présentiel.

A l'exception du Bureau, chacun des membres de chaque instance peut se faire représenter par un membre de la même instance. A cette occasion, il peut lui déléguer son vote par décision écrite.

Ex: Un adhérent peut ainsi se faire représenter en AG par un autre adhérent, et un administrateur peut se faire représenter en CA par un autre administrateur.

# ARTICLE 10 - ASSEMBLEE GENERALE ORDINAIRE 
L'Assemblée générale ordinaire comprend tous les membres de l'association à jour de leurs cotisations le jour de sa tenue et ayant voix délibérative. Peuvent être invitées sans droit de vote, toutes les personnes dont le Conseil d'Administration estime la présence utile.

Elle se réunit une fois par an.

Quinze jours au moins avant la date fixée, les membres de l'association sont convoqués par les soins du secrétaire par voie postale ou numérique (courriel).

L'ordre du jour est fixé par le Conseil d'Administration.

Il figure sur les convocations. 

Le président, assisté des membres du conseil, préside l'assemblée et expose la situation de l'association. 

Le trésorier rend compte de sa gestion et soumet les comptes annuels (bilan, compte de résultat et annexe) à l'approbation de l'assemblée. 

L'Assemblée générale approuve le bilan des activités de l'association et son rapport financier.

Les orientations proposées par le Conseil d'Administration sont présentées et approuvées à cette occasion.

L'Assemblée générale élit les membres sortants du Conseil d'Administration.

Ne peuvent être abordés que les points régulièrement inscrits à l'ordre du jour. Un vote peut être proposé par le Conseil d'Administration sur les résolutions.

Tout membre peut demander à voir un point spécifique inscrit à l'ordre du jour jusque 5 jours avant la tenue de l'Assemblée générale. Le Conseil d'Administration statue sur sa demande.

Les décisions sont prises à la majorité simple des voix des membres présents, et sous réserve que la moitié des membres du Conseil d'Administration soient présents.

Toutes les délibérations sont prises à main levée et lecture est faite des votes par correspondance ou à distance conformément à l'article 10 des présents statuts.

# ARTICLE 11 - ASSEMBLEE GENERALE EXTRAORDINAIRE

Sur proposition du Conseil d’Administration, une Assemblée générale extraordinaire peut être convoquée pour modifier les statuts, prononcer la dissolution de l'association ou lorsque les intérêts de l'association l'exigent.

Les modalités de convocation et de tenue de l'assemblée sont les mêmes que pour l’Assemblée générale ordinaire. 

Les délibérations sont prises à la majorité des deux tiers des membres présents et représentés, sous réserve d'obtenir le quorum de votes de la moitié des membres.


# ARTICLE 12 - CONSEIL D'ADMINISTRATION
L'association est dirigée par un Conseil d’Administration de 3 à 10 personnes comprenant :

- 2/3 de membres élus par les membres fondateurs
- 1/3 de membres élus par l'Assemblée générale

Ils sont mandatés pour trois années, de manière illimitée.

Sur convocation du Président, le Conseil d'Administration se réunit au moins 2 fois par an et chaque fois que ses membres l'estiment nécessaire.

Il fonctionne de manière collégiale et les missions sont réparties entre ses membres.

Les décisions sont prises à la majorité simple et à main levée.

Le Conseil d'Administration est compétent pour prendre toutes décisions relatives à l'association et notamment en matière de finances, projets et choix stratégiques de l'association.

Il établit l'ordre du jour des Assemblées Générales Ordinaires et Extraordinaires.

En cas de vacance exceptionnelle d'un membre, le Conseil d'Administration peut pourvoir lui-même au remplacement du membre jusqu'à ratification du remplacement par la prochaine Assemblée Générale Ordinaire. Une fois approuvé lors de l'Assemblée Générale Ordinaire, il assure la mission d'administrateur jusqu'à la fin du mandat initialement porté par son prédecesseur.


# ARTICLE 13 – LE BUREAU

Chaque année, le Conseil d'Administration élit en son sein, un bureau composé de :
1) Un président ;
2) Un secrétaire général ;
3) Un trésorier. 

Le bureau est élu pour 1 an.

Le bureau est chargé de la mise en oeuvre opérationnelle des activités de l'association.

L’association est représentée en justice par son Président ou toute personne du Conseil d'Administration mandaté par ce dernier, et peut ester en justice selon ces mêmes-conditions.

Le trésorier dresse le rapport financier de l'association. Il assure de la régularité et la sincérité des comptes.

Les membres sortants sont rééligibles.


# ARTICLE 14 – CHARTRE

Une charte des valeurs de l'association est rédigée par les membres fondateurs. Elle peut être modifiée sur proposition du Conseil d'Administration et votée en Assemblée Générale ordinaire.

Elle a pour objet de rappeler les principes essentiels portés par l'association et les valeurs que celle-ci entend défendre dans le cadre de ses activités.

Le non-respect de ces valeurs peut être un motif d'exclusion des membres.

# ARTICLE 15 – RÉGLEMENT INTÉRIEUR

Un règlement intérieur est établi par le Conseil d'Administration.

Il peut être modifié à la majorité des 2/3.

Il comprend notamment et de façon non exhaustive : 

- les modalités d'adhésion et les modalités d'exclusion des membres.
- le montant des cotisations 


# ARTICLE 16 – PREVENTION DES CONFLITS D'INTERET

Lorsqu'un membre d'une des instances (CA, AG, Bureau) est en conflit d'intérêt sur un point soulevé au débat, il ne peut participer aux délibérations et au vote le concernant.

# ARTICLE 17 – DISSOLUTION

Sur proposition du Conseil d'Administration, l'association peut être dissoute par l'Assemblée Générale Extraordinaire selon les modalités prévues à l’article 11.

Un ou plusieurs liquidateurs sont nommés lors de l'Assemblée Générale Extraordinaire.

L'actif, s'il y a lieu, ne peut être en aucun cas dévolu à ses membres. Il doit faire l'objet d'une dévolution à des associations de même nature.

Paris, le 13 novembre 2020.

**Adrien PARROT**, Le Président,

**Antoine LAMER**, Le Trésorier,

**Nicolas PARIS**, Le secrétaire
