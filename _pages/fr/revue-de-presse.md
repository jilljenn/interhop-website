---
layout: presentation
title: Revue De Presse
subtitle: Notre Veille
image_path: /images/projets/newspaper.png
redirect_from:
  - /press
ref: revue-de-presse
lang: fr
---

<a name="HDH"></a>
# Données de Santé / Health Data Hub

[La stratégie du criquet des GAFAM-BATX menace la démocratie et l'économie](https://www.latribune.fr/opinions/tribunes/la-strategie-du-criquet-des-gafam-batx-menace-la-democratie-et-l-economie-855083.html) <em><span style="color:orange;">18 août</span></em>
- "En maîtrisant les données d'un Etat, nous maîtrisons désormais sa population. Une guerre nouvelle génération se déroule sous nos yeux, tout aussi dangereuse et bien plus pernicieuse car silencieuse. En laissant nos données à des entreprises qui dépendent d'Etats impérialistes, nous hypothéquons notre démocratie, nos valeurs et l'avenir de nos entreprises et des citoyens. Nous dépendrons, si nous ne réagissons pas, d'autres Etats et de leur bon vouloir, laissant le peuple dépossédé de sa souveraineté."

[Fichiers sanitaires : un destin tracé vers la surveillance généralisée ?](https://theconversation.com/fichiers-sanitaires-un-destin-trace-vers-la-surveillance-generalisee-141894) <em><span style="color:orange;">10 août</span></em>
- "l’intelligence collective est congédiée au profit d’un gouvernement des experts, par essence incontestable"
- "Les fichiers traitant des données de santé rejoignent alors pleinement les fichiers à visée sécuritaire en abandonnant l’idée d’un consentement de l’individu : ils sont imposés, au nom du bien commun."
- "L’atteinte aux libertés fondamentales est en effet rendue davantage visible par le couplage entre ce premier niveau – le caractère imposé des traitements de données – et un second niveau de la fusée : l’élargissement et l’affaiblissement des finalités."
- "La vie privée devient, de manière paradoxale avec la promulgation du RGPD et les progrès apparemment concédés il y a deux ans, un droit formaliste et individualiste, qui peut céder sans risque face aux enjeux sécuritaires et sanitaires, et qu’il s’agit désormais de démanteler pièce par pièce."

[Sûreté des entreprises. « En matière de souveraineté, on marche sur la tête ! »](http://www.protectionsecurite-magazine.fr/actualite/surete-des-entreprises-en-matiere-de-souverainete-on-marche-sur-la-tete#.Xx0o9MbX1lA.linkedin) <em><span style="color:orange;">22 juillet</span></em>
- "Rappelons que le gouvernement français vient par exemple de prendre la décision d'héberger les informations de santé de millions de Français sur les serveurs de l'américain Microsoft. C’est édifiant ! On marche sur la tête !"
- "Il faudrait d’abord définir ce qu’est une solution souveraine, c’est-à-dire que ses utilisateurs et les contenus qu’ils y déposent échappent au caractère extraterritorial du Cloud Act."
- "Sûreté des entreprises. « En matière de souveraineté, on marche sur la tête ! »"

[Un arrêté autorise la collecte de données de santé en cas d’"alerte sanitaire"](https://www.ticsante.com/story/5260/un-arrete-autorise-la-collecte-de-donnees-de-sante-en-cas-dalerte-sanitaire.html) <em><span style="color:orange;">17 juillet</span></em>
- "Dans une délibération sur le projet d'arrêté datée du 11 juin et publiée le 2 juillet au Journal officiel, la Commission nationale de l'informatique et des libertés (Cnil) estime que plusieurs de ces institutions "ne devraient pas figurer dans l’arrêté". Elle vise les organismes et services de recherche, les DDCSPP, l'ANS, la PDS et l'ATIH, au motif qu'ils agissent en tant que sous-traitant ou destinataire des données au sens du règlement général sur la protection des données (RGPD)."

[Health Data Hub et Covid-19 : la sécurité des données médicales en débat devant le Conseil d'État](https://www.lequotidiendumedecin.fr/actu-pro/politique-de-sante/health-data-hub-et-covid-19-la-securite-des-donnees-medicales-en-debat-devant-le-conseil-detat) <em><span style="color:orange;">12 juin</span></em>

[Les données de santé de la population française, collectées massivement, seront-elles vraiment protégées ?](https://www.bastamag.net/Donnees-de-sante-Microsoft-Gafam-Health-Data-Hub-Covid-fichier-Big-Data) <em><span style="color:orange;">11 juin</span></em>
- "La mutuelle Malakoff Médéric a aussi pu avoir accès à des données pour « mesurer et comprendre les restes à charge réels des patients »"
- "Ainsi OPBI Santé, fondée par deux « consultants en organisation hospitalière », souhaite par exemple accéder aux données pour déterminer les durées de séjour qui seraient les plus efficientes pour chaque type de pathologie. « Des durées atteignables et pertinentes, mais nécessitant les meilleures pratiques organisationnelles », vante la start-up."

[Health Data Hub : des organisations dénoncent un passage en force du projet dans le contexte d'urgence sanitaire](https://www.zdnet.fr/actualites/health-data-hub-des-organisations-denoncent-un-passage-en-force-du-projet-dans-le-contexte-d-urgence-sanitaire-39905045.htm) <em><span style="color:orange;">11 juin</span></em>
- "Les requérants demandent au Conseil d'Etat de suspendre l'arrêté en date du 21 avril 2020 confiant les données de santé liées au Covid-19 au Health Data Hub"
- "Les requêtes déposées au Conseil d'Etat portent sur deux aspects de la protection des données : d'un côté, le risque de voir un Etat tiers (les Etats-Unis en l'occurrence) accéder aux données de santé françaises et, de l'autre, l'absence d'information préalable à destination du public (prévue par le RGPD)."
- "La plateforme sera alimentée au cas par cas, au fur et à mesure des projets, rappelle Stéphanie Combes. On y trouve pour l'heure les données du réseau "Oscour" (Organisation de la surveillance coordonnée des urgences) issues de Santé Publique France, permettant de suivre les passages aux urgences pendant l'épidémie."

[Health Data Hub : « Le choix de Microsoft est un contresens industriel ! »](https://amp.lepoint.fr/2379394?utm_term=Autofeed&utm_medium=Social&utm_source=Twitter&Echobox=1591814194&__twitter_impression=true) <em><span style="color:orange;">10 juin</span></em>
- "Il s'agit d'un signal politique inquiétant et aussi d'un contresens industriel ! Le choix de la société Microsoft pour assurer l'hébergement du Health Data Hub a été effectué sans créer un appel d'offres spécifique et a été mis en avant pour des raisons de conformité avec les prérequis de ce projet. Ces prérequis doivent aujourd'hui être remis en question."
- "Mais Microsoft aura à cœur de se rendre indispensable ! En effet, si la réversibilité du choix de Microsoft a souvent été soulignée, il faut se rappeler que le « cœur de métier » des sociétés qui assurent l'hébergement et le traitement en masse des données est justement d'ajouter progressivement des fonctions « propriétaires » afin d'empêcher que leurs clients ne puissent migrer facilement vers d'autres plateformes."
- "En raison de la diversité et du volume des données qu'il devrait rassembler, le Health Data Hub constitue l'un des plus importants projets d'administration électronique jamais menés en France et certainement l'un des plus stratégiques pour les géants des technologies."

[Le Health Data Hub attaqué devant le Conseil d’Etat](https://www.mediapart.fr/journal/france/090620/le-health-data-hub-attaque-devant-le-conseil-d-etat?onglet=full) <em><span style="color:orange;">9 juin</span></em>
- "Une  quinzaine  de  personnalités  et  d’organisationsont  déposé  un  référé-liberté  contre  le  déploiement,accéléré  au  nom  de  l’état  d’urgence  sanitaire,  dela  nouvelle  plateforme  de  santé  devant  centraliserl’intégralité   de   nos   données   de   santé   et   dontl’hébergement a été confié à Microsoft."
- "Ils ont cette fois été rejoints par le collectif InterHop,composé  de  professionnels  du  secteur  de  la  santé  etde l’informatique médicale, mobilisé depuis près d’unan  contre  le  projet  mais  également  par  le  médecinDidier  Sicard,  ancien  président  du  Comité  nationalconsultatif  d’éthique,  le  professeur  Bernard  Fallery,spécialiste  des  systèmes  d’information,  le  Syndicatnational  des  journalistes  (SNJ),  le  Syndicat  de  lamédecine générale (SMG), l’Union française pour unemédecine libre (UFML), la représentante des usagersdu conseil de surveillance de l’APHP, l’Observatoirede la transparence dans les politiques de médicaments,l’Union générale des ingénieurs, cadres et techniciensCGT  (UGICT-CGT)  et  l’Union  fédérale  médecins,ingénieurs,  cadres,  techniciens  CGT  Santé  et  Actionsociale (UFMICT-CGT Santé et Action sociale)."
- "Health Data Hub « porte une atteinte grave et sûrementirréversible  aux  droits  de  67  millions  d’habitantsde   disposer   de   la   protection   de   leur   vie   privéenotamment  celle  de  leurs  données  parmi  les  plusintimes,  protégées  de  façon  absolue  par  le  secretmédical : leurs données de santé »"

[La France mise 500 millions pour protéger ses start-up de l’appétit des Gafa](https://www.lefigaro.fr/secteur/high-tech/la-france-mise-500-millions-pour-proteger-ses-start-up-de-l-appetit-des-gafa-20200604) <em><span style="color:orange;">5 juin</span></em>
- "Bruno Le Maire et Cédric O vont présenter un plan destiné à préserver la souveraineté nationale dans le numérique, dont le montant total s’élève à 1,2 milliard d’euros."
- "Or, dans certains domaines, la France, pays aux 13.000 start-up, disposent de pépites qui sont autant de «proies», pouvant être rachetées par des géants du numériques extra-territoriaux, essentiellement américains"

[L'Etat choisit Microsoft pour les données de santé et crée la polémique](https://www.lesechos.fr/tech-medias/hightech/letat-choisit-microsoft-pour-les-donnees-de-sante-et-cree-la-polemique-1208376) <em><span style="color:orange;">5 juin</span></em>
- "Le gouvernement français a pris la décision d'héberger les informations de santé de millions de Français sur les serveurs de l'américain Microsoft, au détriment d'OVH, une société française."
- "La polémique ne pouvait pas éclater à un pire moment. Quelques jours avant de lancer avec l'Allemagne le projet d'informatique en ligne européenne Gaia-X pour la protection des données dans le « cloud computing », le gouvernement français doit défendre sa décision d'héberger les informations de santé de millions de Français sur les serveurs de l'américain Microsoft…"
- "« Il faut être rapide parce que le Health Data Hub sauve des vies »"

[Opinion Le Health Data Hub, un outil à la pointe de l'innovation numérique publique](https://www.lesechos.fr/idees-debats/cercle/opinion-le-health-data-hub-un-outil-a-la-pointe-de-linnovation-numerique-publique-1208487) <em><span style="color:orange;">5 juin</span></em>

[« La politique publique des données de santé est à réinventer »](https://www.lemonde.fr/idees/article/2020/06/04/la-politique-publique-des-donnees-de-sante-est-a-reinventer_6041706_3232.html) <em><span style="color:orange;">4 juin</span></em>
- "Il faut donc remettre à plat le sujet, avec des orientations claires : rétablir la confiance et définir une
stratégie ; couvrir l’ensemble du champ sanitaire et médico-social ; simplifier l’accès pour permettre
d’aller vite ; développer l’utilisation des données en temps réel pour repérer les problèmes
émergents ; ancrer l’architecture technique dans un écosystème décentralisé, respectueux des acteurs
et propice à une maîtrise « souveraine » de l’hébergement des données ; garantir le respect du secret
médical et du droit à la vie privée ; favoriser l’émergence de nouvelles technologies dans le respect
d’une éthique et d’une déontologie exigeantes"


[Pour une souveraineté européenne de la santé](https://forumatena.org/pour-une-souverainete-europeenne-de-la-sante/) <em><span style="color:orange;">27 mai</span></em>
- "Peut-on compter sur la pseudonymisation quand les attributs de chaque individu se comptent par milliers ?"
- "Le comité (CESREES) en charge de se prononcer sur le caractère d’intérêt public des projets n’exercera son filtre que sporadiquement : seule une requête du ministre de la Santé, de la CNIL ou du CESREES lui-même provoquera l’examen. La CNIL avait pourtant recommandé une étude systématique."

[Données de santé : l’arbre StopCovid qui cache la forêt Health Data Hub](https://theconversation.com/donnees-de-sante-larbre-stopcovid-qui-cache-la-foret-health-data-hub-138852) <em><span style="color:orange;">25 mai</span></em>
- "C’est une question de politique nationale, déjà soulevée dans The Conversation France, puisqu’il s’agit de faire gérer un bien public par un acteur privé, et sans espoir de réversibilité. Mais aussi une question politique de souveraineté numérique européenne puisque cet acteur étasunien se trouve soumis au Cloud Act, loi de 2018 qui permet aux juges américains de demander l’accès aux données sur des serveurs situés en dehors des États-Unis."

[Health Data Hub : pourquoi cette plateforme inquiète tant ?](https://www.presse-citron.net/health-data-hub-pourquoi-cette-plateforme-inquiete-tant/) <em><span style="color:orange;">18 mai</span></em>
- "Le rassemblement de ces données sera complété par deux fichiers récents liés au Covid-19, le SIDEP qui regroupe les données de Laboratoire et le Contact Covid où sont inscrites les personnes potentiellement contaminées au Covid-19. Autrement dit un nombre considérable de données de santé seront stockées via ce nouveau système."
- "L’objectif affiché de Health Data Hub serait de rendre le système de santé plus efficace et d’aider à la gestion de la crise sanitaire que nous traversons actuellement. À terme, toutes les données enregistrées sur le site de la sécurité sociale seront automatiquement intégrées au fichier."

[Health Data Hub: pourquoi le fichier national qui abritera nos données de santé suscite des craintes](https://www.bfmtv.com/tech/health-data-hub-pourquoi-le-fichier-national-qui-abritera-nos-donnees-de-sante-suscite-des-craintes-1913849.html) <em><span style="color:orange;">17 mai</span></em>

[Health Data Hub : nos données de santé vont-elles être livrées aux Américains ?](https://www.01net.com/actualites/health-data-hub-nos-donnees-de-sante-vont-elles-etre-livrees-aux-americains-1913351.html) <em><span style="color:orange;">14 mai</span></em>
- "le gouvernement passe en force son projet de plate-forme sanitaire géante au nom de l'état d'urgence. Problème : les données seraient hébergées par Microsoft qui pourrait sur seul ordre des États-Unis... les transférer outre-Atlantique"

[Thomas Gomart (IFRI) : « Le Covid-19 accélère le changement de mains de pans entiers de l'activité économique au profit des plateformes numériques »](https://www.lesechos.fr/amp/1201806) <em><span style="color:orange;">13 mai</span></em>
- "les données des consommateurs européens ont été exploitées par les plateformes américaines bien au-delà de ce que les utilisateurs pouvaient savoir. L’affaire Snowden, en 2013, a révélé au grand jour certains mécanismes."
- "Les plateformes américaines sont parties prenantes du complexe militaro-numérique. Tout en étant des entreprises privées versant peu de dividendes mais investissant beaucoup, elles sont des vecteurs de la puissance américaine. En peu de temps, elles ont inventé et offert des services auxquels les Européens ne veulent pas renoncer. Le danger, c’est de se focaliser sur le service sans vouloir voir l’architecture de puissance."
- "L’enjeu fondamental pour les Européens est d’être en mesure de conserver leur autonomie de pensée."

[Données de santé : pourquoi (et comment) vont-elles être hébergées sur des serveurs de Microsoft ?](https://www-lci-fr.cdn.ampproject.org/c/s/www.lci.fr/amp/sante/donnees-de-sante-pourquoi-et-comment-vont-elles-etre-hebergees-sur-des-serveurs-de-microsoft-2153527.html) <em><span style="color:orange;">11 mai</span></em>
- "un flicage éhonté de vos informations de santé"

[La Cnil s’inquiète d’un possible transfert de nos données de santé aux Etats-Unis](https://www.mediapart.fr/journal/france/080520/la-cnil-s-inquiete-d-un-possible-transfert-de-nos-donnees-de-sante-aux-etats-unis) <em><span style="color:orange;">8 mai</span></em>
- "La Cnil aurait-elle alors mal lu le contrat ? « Je ne dis pas ça. Mais je trouve que les faits sont un peu détournés. En tout cas, nous avons bien indiqué que les données ne pourront pas être transférées. Je peux même vous dire que c’est à la page 11 du contrat.»"

[L’épineuse question des données numériques de santé](https://theconversation.com/lepineuse-question-des-donnees-numeriques-de-sante-131586)
- "Quant au temps dévolu à la saisie des données médicales, il a stagné voire nettement augmenté, selon les trois témoins interrogés. Cette perte de temps est vécue d’autant plus mal que la dégradation des conditions de travail (sur fond de sous-effectif et de sous-investissement chroniques) place les employés dans une course à la montre permanente."
- "Or l’hébergement des données a été confié à Microsoft, et l’intégralité des données stockées sera, en vertu du Cloud Act, accessible en toute légalité… aux agences de renseignement américaines."
- "La mise à disposition de données médicales par le biais d’accords commerciaux soumis au secret pose à cet égard de graves problèmes de principe."

[Le Health Data Hub ou le risque d’une santé marchandisée](https://lvsl.fr/le-health-data-hub-ou-le-risque-dune-sante-marchandisee/)
- "Il semble qu’ici se loge une fois de plus la contradiction de fond entre la logique du soin inconditionnel propre au secteur public ainsi qu’au serment d’Hippocrate et les exigences d’efficacité et de rentabilité aujourd’hui dénoncées par les personnels médicaux et hospitaliers à travers leurs mouvements de grève et leurs réactions à la crise du Covid-19"

[Données de santé: l’Etat accusé de favoritisme au profit de Microsoft](https://www.mediapart.fr/journal/france/110320/donnees-de-sante-l-etat-accuse-de-favoritisme-au-profit-de-microsoft)
- "Le non-respect des principes d’égalité et de transparence dans le choix de Microsoft Azure"

[Pour des données de santé au service des patients](https://interhop.org/donnees-de-sante-au-service-des-patients/)
- "Selon un récent sondage, l’hôpital est même l’institution en laquelle les Français ont le plus confiance. Quel serait l’impact d’une perte de confiance si des fuites de données massives étaient avérées ?"

[«Health Data Hub»: le méga fichier qui veut rentabiliser nos données de santé](https://www.mediapart.fr/journal/france/221119/health-data-hub-le-mega-fichier-qui-veut-rentabiliser-nos-donnees-de-sante)
- "Le gouvernement est-il en train de mettre en place un « big brother médical » et d’offrir nos données de santé aux géants du numérique ?"

[Opinion | Soignons nos données de santé](https://www.lesechos.fr/idees-debats/cercle/opinion-soignons-nos-donnees-de-sante-1143640)
- "Une fois brisée, la confiance ne se reforme plus"

[Données de santé & intelligence artificielle : rencontre avec Emmanuel Bacry](https://www.actuia.com/actualite/donnees-de-sante-intelligence-artificielle-rencontre-avec-emmanuel-bacry/)
- "J’aurais tendance à penser qu’il faut ouvrir totalement les données dans un endroit totalement sécurisé, et ensuite on contrôle ce qui se passe."

[Comment les GAFAM et Capgemini s'invitent dans la mine d'or des données médicales](https://www.lalettrea.fr/action-publique_executif/2019/02/18/comment-les-gafam-et-capgemini-s-invitent-dans-la-mine-d-or-des-donnees-medicales,108345136-ge0)

<a name="appelOffre"></a>
# Marché public / appel d'offre
[La loi Asap est publiée au Journal officiel](https://www.lagazettedescommunes.com/711842/la-loi-asap-est-publiee-au-journal-officiel/)
- L’article 131 de la loi ajoute l’intérêt général comme motif de recours à la passation de marchés publics sans publicité ni mise en concurrence.

[Le logiciel libre arrive au sein de la Centrale d'achat de l'informatique hospitalière](https://www.nextinpact.com/article/43372/le-logiciel-libre-arrive-au-sein-centrale-dachat-informatique-hospitaliere) <em><span style="color:orange;">14 août</span></em>

[INFO OBS. Comment Microsoft a obtenu les données de santé des Français](https://www.nouvelobs.com/economie/20200731.OBS31754/info-obs-comment-microsoft-a-obtenu-les-donnees-de-sante-des-francais.html) <em><span style="color:orange;">31 juillet</span></em>
- "Plutôt qu’une publication du cahier des charges et du budget, ce projet en gestation depuis juin 2018 est passé par une procédure accélérée, discrète et sans mise en concurrence, par le biais de l’Union des Groupements d’Achats publics (Ugap), la centrale d’achat de l’Etat."
- "une aubaine pour Microsoft dont la candidature à l’hébergement du « Hub » était gérée par son directeur technique, Bernard Ourghanlian, [est] membre du Syntec. Et depuis, Guy Mamou-Mani ne cesse de vanter le choix de Microsoft."

[Déclaration commune sur les enjeux du numérique en santé](https://www.dsih.fr/article/3772/declaration-commune-sur-les-enjeux-du-numerique-en-sante.html) <em><span style="color:orange;">12 mai</span></em>
- "Une dizaine d’organisations représentant les usagers de la santé, les médecins et les industriels affirme d’une seule voix les principes de construction d’un cadre national en matière de numérique en santé."

[Protection des données de santé : MORIN-DESAILLY Catherine](http://videos.senat.fr/video.1710660_5f10400c7efbf.seance-publique-du-16-juillet-2020-apres-midi?timecode=16471000) <em><span style="color:orange;">16 juillet</span></em>
- Réponse de Monsieur Olivier Véran : "Les donnees de sante doivent rester de sécurisation et de gestion francaise." "Oui il aura un appel d'offre pour le fonctionnement au quotidien."

[https://www.economie.gouv.fr/direct-video-conference-presse-sur-application-stopcovid-23-juin#](https://www.economie.gouv.fr/direct-video-conference-presse-sur-application-stopcovid-23-juin#) <em><span style="color:orange;"> 23 juin</span></em>
- Monsieur Cédric O : "Il sera normal que dans les mois qui vienne, nous puissions lancer un appel d'offre pour avoir un choix plus large et des spécifications qui permettront à chacun de se positionner"

[Émission Libre à vous ! diffusée mardi 28 janvier 2020 sur radio Cause Commune](https://april.org/libre-a-vous-radio-cause-commune-transcription-de-l-emission-du-28-janvier-2020)
- "Donc on a commencé à faire un tour de piste et à discuter avec pas mal d’interlocuteurs, notamment des industriels français type Atos, Thalès, etc., et on est arrivés assez rapidement à la conclusion qu’aucun d’entre eux, en tout cas à l’époque, donc au tout début 2019, n’était compatible avec la certification hébergement des données de santé, qu’on avait jugée nécessaire pour notre projet, même si elle n’est pas obligatoire contrairement au Référentiel de sécurité du système national des données de santé que j’ai déjà évoqué et qui s’applique. "

<a name="extraterritorialite"></a>
# Extraterrorialité américaine / CloudAct - RGPD

[La nouvelle guerre mondiale des Américains](https://www.lajauneetlarouge.com/la-nouvelle-guerre-mondiale-des-americains/)
- "Il suffit par exemple que le dollar ait été utilisé dans une transaction litigieuse pour donner compétence aux Américains, ou encore que les personnes mises en cause aient utilisé une adresse e-mail fournie par Google."
- "Plusieurs dizaines de milliards de dollars d’amendes ont été réclamées à des entreprises françaises et européennes, au motif que leurs pratiques commerciales, leurs clients ou certains de leurs paiements ne respectaient pas le droit américain, alors même que ces entreprises se conformaient au droit de leur pays."
- "Aux États-Unis, le droit est devenu une arme économique."

[Don’t expect new EU-US data transfer deal anytime soon, Reynders says](https://www.euractiv.com/section/data-protection/news/dont-expect-new-eu-us-data-transfer-deal-anytime-soon-reynders-says/) <em><span style="color:orange;">4 septembre</span></em>
- "Il n'y aura pas de "solution miracle" à un accord révisé sur le transfert de données entre l'UE et les États-Unis, suite à la décision des juges européens de juillet d'annuler l'accord sur le bouclier de protection de la vie privée, a déclaré le commissaire européen à la justice, Didier Reynders, aux députés européens."
- "Un arrêt de la Cour européenne de justice du 16 juillet a estimé que le régime de surveillance américain ne permet pas un degré suffisant de protection des données européennes, les exposant ainsi à un risque qui violerait les droits accordés aux citoyens en vertu du règlement général sur la protection des données de l'UE (RPGD)."
- "La section 702 de la loi américaine sur la surveillance du renseignement étranger (FISA) autorise l'Agence nationale de sécurité à recueillir des renseignements étrangers appartenant à des non-Américains situés en dehors des États-Unis, et préoccupe depuis longtemps les militants de la protection de la vie privée en Europe."
- "Schrems [...] a récemment reçu une lettre de Facebook, "qui dit qu'il continuera à transférer toutes les données" des utilisateurs européens de Facebook entre le bloc et les États-Unis.""

[Non, le CLOUD Act n'est pas ce que vous croyez. Vraiment pas.](https://francoischarlet.ch/2020/cloud-act-pas-ce-que-vous-croyez/) <em><span style="color:orange;">23 août</span></em>
- "On peut donc déduire de ce qui précède que le CLOUD Act n’étend pas les pouvoirs des autorités américaines de poursuite pénale en matière de perquisition, et que l’étendue du droit américain reste inchangée à cet égard. Il ne fait que codifier une pratique qui dure depuis que le SCA a été adopté en 1986."
- "L’innovation première du CLOUD Act est de permettre que des accords bilatéraux entre les Etats-Unis et des pays étrangers suppriment les interdictions susmentionnées et ensuite de permettre aux autorités de ces pays d’accéder au contenu des communications en s’adressant directement aux ISPs basés aux États-Unis"

[Peut-on encore éviter le piège du CLOUD Act](https://www.solutions-magazine.com/peut-on-encore-eviter-le-piege-du-cloud-act/) <em><span style="color:orange;">21 juillet</span></em>
- "Cloud Act n'est pas compatible avec le RGPD. [...] Le FISA (Foreign Intelligence Service Act), qui est largement utilisé, permet de mettre en place une surveillance sur les données d'étrangers, même hébergées hors-usa. Il n'est pas exigé que cela soit justifié par une enquête en cours. Le but de la surveillance peut être purement politique ou commercial."

[Teams : pas de chiffrement bout en bout, Microsoft s’explique](https://www-lemagit-fr.cdn.ampproject.org/c/s/www.lemagit.fr/actualites/252486427/Teams-pas-de-chiffrement-bout-en-bout-Microsoft-sexplique?amp=1) <em><span style="color:orange;">21 juillet</span></em>
- "Teams pour des réunions audio et/ou vidéo ou même des milliers lors des événements live n’est guère réaliste sur le plan technique avec le chiffrement de bout en bout"
- "Par ailleurs le directeur technique estime que le déchiffrement des données sur les serveurs est « indispensable pour permettre la mise en place de dispositifs de type enquête numérique [N.D.R. : traduction de digital forensics analysis], recherche de contenu, conservation et audit ». Autant de fonctionnalités qui compliqueraient, voire rendraient impossible un chiffrement de bout en bout."
- "Microsoft entend donc bien respecter les lois qui régissent son activité, dont le CLOUD Act, mais assure dans le même temps conserver « le droit de s’opposer et de contester des requêtes lorsqu’elles seraient manifestement contraires à des lois locales.»"

[Google, Apple, Facebook et Amazon sermonnés par les politiques américains](https://www.lemonde.fr/economie/article/2020/07/30/etats-unis-google-apple-facebook-et-amazon-sermonnes-par-les-politiques-americains_6047655_3234.html) <em><span style="color:orange;">30 juillet</span></em>
- "un ancien employé d’Amazon : « [Les chefs] nous disent juste : “ne vous servez pas dans les données.” Mais c’est un véritable magasin de bonbons, tout le monde a accès à tout ce qu’il veut. »"

[Privacy Shield : le Luxembourg bloque la route vers les États-Unis](https://www.dalloz-actualite.fr/node/iprivacy-shieldi-luxembourg-bloque-route-vers-etats-unis) <em><span style="color:orange;">29 juillet</span></em>
- "Des éléments transmis à la Cour ressort clairement que les citoyens européens ne bénéficient d’aucune voie de droit contre les autorités américaines ni devant les juridictions américaines"
- "Le transfert des données à caractère personnel vers les Etats-Unis n’est pas possible car il ne présentera pas les garanties appropriées, aussi précis qu’on puisse l’être dans ses stipulations contractuelles"
- "La première solution pratique à envisager est probablement celle de cesser les relations commerciales avec les entreprises américaines, dont les accords étaient basés sur le BPD, et de les remplacer par des entreprises européennes."

[L'annulation du Privacy Shield doit s'appliquer immédiatement, tranche la Cnil européenne](https://www.usine-digitale.fr/article/l-annulation-du-privacy-shield-doit-s-appliquer-immediatement-tranche-la-cnil-europeenne.N989069) <em><span style="color:orange;">27 juillet</span></em>
- "Aucun délai de grâce ne sera accordé aux entreprises européennes qui transfèrent des données vers les Etats-Unis, rapporte le CEPD. L'organe de protection des données au niveau de l'Union européenne prévient que tous les transferts effectués sur la base du Privacy Shield, annulé par le juge européen, sont désormais illégaux."
- "A ce stade, tout transfert vers les Etats-Unis comporte un risque"

[Frequently Asked Questions on the judgment of the Court of Justice of the European Union in Case C-311/18 -Data Protection Commissioner v Facebook Ireland Ltd and Maximillian Schrems](https://edpb.europa.eu/sites/edpb/files/files/file1/20200724_edpb_faqoncjeuc31118.pdf) <em><span style="color:orange;">23 juillet</span></em>
- "La CJUE a estimé que les exigences du droit américain, et en particulier certains programmes permettant l'accès des autorités publiques américaines aux données personnelles transférées de l'UE vers les États-Unis à des fins de sécurité nationale, entraînent des limitations de la protection des données personnelles qui ne sont pas circonscrites de manière à satisfaire à des exigences essentiellement équivalentes à celles requises par le droit de l'UE, et que cette législation n'accorde pas aux personnes concernées des droits de recours devant les juridictions contre les autorités américaines.
Du fait de l’ampleur de l’atteinte portée aux droits fondamentaux des personnes dont les données sont transférées vers ce pays tiers, la CJUE a déclaré la décision d'adéquation du Privacy Shield invalide.
"
- "Les transferts effectués sur la base de ce cadre juridique sont illégaux"

[L'UE sabre le Privacy Shield, l'accord qui autorisait le transfert des données vers les Etats-Unis](https://www.usine-digitale.fr/editorial/l-ue-sabre-le-privacy-shield-l-accord-qui-autorisait-le-transfert-des-donnees-aux-etats-unis.N986549) <em><span style="color:orange;">16 juillet</span></em>
- "En premier lieu, le juge européen estime que les programmes de surveillance américains ne sont pas compatibles avec les principes du Règlement général sur la protection des données (RGPD)"
- "En second lieu, la Cour affirme que "les citoyens européens n'ont pas de recours effectifs aux Etats-Unis" leur permettant de maîtriser pleinement leurs données personnelles."

[La Cour invalide la décision 2016/1250 relative à l'adéquation de la protection assurée par le bouclier de protection des données UE-États-Unis](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf) <em><span style="color:orange;">16 juillet</span></em>
- "Selon  la  Cour, les limitations  de  la  protection  des  données  à  caractère  personnel  qui découlent de la réglementation interne des États-Unis portant sur l’accès et l’utilisation, par les autorités publiques américaines, de telles données transférées depuis l’Union vers ce pays tiers,  et  que  la  Commission  a  évaluées  dans  la  décision 2016/1250, ne  sont  pas  encadrées d’une  manière  à  répondre  à  des  exigences  substantiellement  équivalentes  à  celles requises, en droit de l’Union, par le principe de proportionnalité, en ce que les programmes de surveillance fondés sur cette réglementation ne sont pas limités au strict nécessaire."

[Résultats de l’enquête par les institutions européennes concernant l’utilisation des produits et services Microsoft](https://edps.europa.eu/sites/edp/files/publication/20-07-02_edps_paper_euis_microsoft_contract_investigation_en.pdf) <em><span style="color:orange;">2 juillet</span></em>
- Microsoft divulguerait des données personnelles (y compris les données sur les clients, les données sur les administrateurs, les données de paiement et les données d’assistance) à des tiers, y compris les services répressifs ou d’autres agences gouvernementales.

[Zoom : nouveau cafouillage, nouvelle polémique](https://www.lemagit.fr/tribune/Zoom-nouveau-cafouillage-nouvelle-polemique) <em><span style="color:orange;">5 juin</span></em>
- "Pour les utilisateurs gratuits, nous ne voudrons pas permettre [le chiffrement de bout en bout]. Parce que nous voulons pouvoir travailler avec le FBI et les autorités locales, au cas où certaines personnes utiliseraient Zoom à mauvais escient."

[Bruno Le Maire, vent debout contre le Cloud Act américain](https://www.lesechos.fr/tech-medias/hightech/bruno-le-maire-vent-debout-contre-le-cloud-act-americain-991515)
- "« Plus on creuse, plus c'est inquiétant », notait en janvier Guillaume Poupard, le directeur général de l'Agence nationale de la sécurité des systèmes d'information. Au départ rassurés par la présence d'un juge dans le dispositif - ce qui distingue le Cloud Act des scandales mis à jour par Edward Snowden il y a cinq ans -, les officiels sont plus circonspects depuis qu'ils ont compris qui pouvait être considéré comme exerçant cette fonction aux Etats-Unis. Beaucoup de fonctionnaires, au département du commerce par exemple, ont ce titre. Parfois, certains observateurs vont jusqu'à citer « le shérif du coin »."

[Mme Denis : CloudAct vs. RGPD](http://www.assemblee-nationale.fr/dyn/15/comptes-rendus/csbioeth/l15csbioeth1819026_compte-rendu#)
- "S´agissant du Cloud-Act [...] ce qui est certain est qu´il y a un conflit de loi sur l´article 48 du RGPD"

[Évaluation du CCBE de la loi CLOUD Act des États-Unis](https://www.ccbe.eu/fileadmin/speciality_distribution/public/documents/SURVEILLANCE/SVL_Position_papers/FR_SVL_20190228_CCBE-Assessment-of-the-U-S-CLOUD-Act.pdf)
- "En  tant  que  destinataires  d'un  mandat  en  vertu  du CLOUD  Act,  les  entreprises  technologiques  se  trouveront entre deux lois contradictoires sur les données. Étant donné que le RGPD limite strictement les circonstances dans lesquelles les données peuvent être légalement transférées à des pays tiers et prévoit  de  lourdes  amendes  en  cas  de  violation  (allant jusqu'à  4  %  du  chiffre  d'affaires  d'une  entreprise), les entreprises sont prises entre le non-respect d'un mandat des États-Unis (pour violation d'une ordonnance du CLOUD Act) et le risque de sanctions monétaires voire pénales importantes (pour violation des dispositions du RGPD)"
- "Le CLOUD Act menace de compromettre l'inviolabilité du conseil juridique sans offrir aux avocats ou à leurs clients aucune garantie procédurale pour protéger le secret professionnel accordé par le droit de l’UE."

[Rapport Gauvain](https://www.vie-publique.fr/sites/default/files/rapport/pdf/194000532.pdf) (p28-30)
- "Les données à caractère personnel des personnes physiques, comme les données des personnes morales sont concernées"
- "Au total, quoi qu’il en soit, le champ des infractions concernées est très vaste et comprend sans aucun  doute  l’ensemble  des  infractions  de  nature  économique,  commerciale  et  financière susceptibles d’affecter les entreprises françaises"

[Rapport Longuet](http://www.senat.fr/notice-rapport/2019/r19-007-1-notice.html)
- "pour répondre à cette exigence [de maîtrise des données], mais aussi afin de réaliser, autant que possible, des économies d'acquisition, de gestion, de maintenance et de formation, plusieurs administrations ont fait le choix de développer leurs propres solutions informatiques, à partir de logiciels dont les codes sources sont publics. C'est, par exemple, le cas de la Gendarmerie qui, depuis 2009, a équipé les 80.000 postes informatiques de ses services de solutions informatiques libres qui lui ont permis de regagner son indépendance et sa souveraineté vis-à-vis des éditeurs privés. Il serait très utile de réaliser rapidement le bilan de cette expérience unique et d'évaluer les possibilités de son extension à d'autres ministères."

[Arnaud Coustillière, directeur général de la DGNUM](https://www.dailymotion.com/video/x7mhqgf)
- "On a besoin de partenaires de confiance"
  - "un cadre juridique sans extraterritorialité"
  - "une communauté de destin"

[Microsoft devient le premier hébergeur de données de santé certifié en France](https://www.usinenouvelle.com/article/microsoft-devient-le-premier-hebergeur-de-donnees-de-sante-certifie-en-france.N765904)
- "Microsoft s’investit beaucoup dans le big data et le machine learning en santé en France"

[Selon l’avocat général Saugmandsgaard Øe, la décision 2010/87/UE de la Commission relative aux clauses contractuelles types pour le transfert de données à caractère personnel vers des sous-traitants établis dans des pays tiers est valide](https://curia.europa.eu/jcms/upload/docs/application/pdf/2019-12/cp190165fr.pdf)

[Le Cloud Act, la riposte américaine au RGPD européen](https://www.lesechos.fr/idees-debats/cercle/le-cloud-act-la-riposte-americaine-au-rgpd-europeen-139429)
- "Mais toutes ces bonnes intentions n'ont pas pesé lourd face à la logique d'extraterritorialité (judiciaire et numérique) que prône l'administration américaine, laquelle a ratifié, toute juste deux mois avant la mise en oeuvre du RGPD, son Cloud Act, en élargissant ainsi ses prérogatives au monde entier. Le RGPD est de facto piétiné par le Cloud Act : particulièrement dans son article 48, qui précise notamment que les demandes de données par un pays tiers doivent être effectuées dans le cadre d'un accord international… alors que la nouvelle loi américaine est parfaitement unilatérale."

[Question N° 101223 de Mme Isabelle Attard (Non inscrit - Calvados )](http://questions.assemblee-nationale.fr/q14/14-101223QE.htm)
- "« l'ensemble des produits américains doivent obtenir l'aval de la National security agency (NSA) pour être exportés ». La NSA introduit systématiquement des portes dérobées ou « backdoors » dans les produits logiciels. Un système d'information et de communication reposant majoritairement sur des produits américains comme Microsoft serait vulnérable car susceptible d'être victime d'une intrusion de la NSA dans sa totalité."

<a name="gafam"></a>
# Importance de la santé pour les Gafam
[How the “Big 4” Tech Companies Are Leading Healthcare Innovation](https://healthcareweekly.com/how-the-big-4-tech-companies-are-leading-healthcare-innovation/)
- "Selon Business Insider, Google Venture investit environ un tiers de ses fonds dans des startups de la santé et des sciences de la vie."

[Owkin annonce une extension de sa série A et atteint 18 millions de dollars levés](https://www.maddyness.com/2018/05/23/medtech-owkin-leve-11-millions-mettre-lia-service-de-recherche-medicale/)
- "Owkin annonce l’extension de sa série A auprès de Google Venture"

<a name="alternatives"></a>
# Alternatives au HDH-Microsoft
[Teralab](https://www.teralab-datascience.fr/)
- "DATA SCIENCE FOR EUROPE : Plateforme d'intelligence artificielle et de big data"

[Centre d'accès sécurisé aux données - CASD](https://www.casd.eu/technologie/securite-certifications/)

[Elixir](https://elixir-europe.org/services/covid-19)
- "ELIXIR est une organisation intergouvernementale qui rassemble des ressources en sciences de la vie de toute l'Europe. Ces ressources comprennent des bases de données, des outils logiciels, du matériel de formation, du stockage en nuage et des superordinateurs. L'objectif d'ELIXIR est de coordonner ces ressources afin qu'elles forment une infrastructure unique. "

[GAIA-X](https://www.dotmagazine.online/issues/on-the-edge-building-the-foundations-for-the-future/gaia-x-a-vibrant-european-ecosystem)
- "GAIA-X: développer un écosystème européen dynamique"
- "GAIA-X est un projet ambitieux et opportun qui créera une infrastructure de données européenne efficace, sécurisée, distribuée et souveraine dans le dialogue entre l'État, l'industrie et la recherche"
- "Cependant, GAIA-X ne vise pas à construire le prochain fournisseur mondial de cloud, mais plutôt à permettre la participation des nombreux fournisseurs spécialisés à travers l'Europe et à les inclure dans une infrastructure de données distribuée et interconnectée"

[Plateforme bigdata du partenariat  entre  la  Caisse  Nationale d’Assurance Maladie et l’Ecole polytechnique](https://stephanegaiffas.github.io/files/papers/2018-01-01-medicine-academy.pdf)
- "Sachant que tous les outils utilisés dans ce projet sont des standards du monde du big data , le passage à l’échelle d evient un problème mineur. De plus, fait notable : ils sont tous libres de droits ( open source ) , donc par définition très ouverts à une recherche méthodologique sans limite."

[Le CERN revient sur son projet Microsoft Alternatives (MALt) pour passer sur des solutions open source](https://www.nextinpact.com/brief/le-cern-revient-sur-son-projet-microsoft-alternatives--malt--pour-passer-sur-des-solutions-open-source-9025.htm)
- "les conditions financières attractives qui avaient initialement attiré les responsables de services du CERN ont peu à peu disparu pour être remplacées par des systèmes basés sur des licences et des modèles commerciaux adaptés au secteur privé"

[OVH-Outscale : le cloud souverain vraiment ressuscité ?](https://www.lemondeinformatique.fr/actualites/lire-ovh-outscale-le-cloud-souverain-vraiment-ressuscite-76657.html)

[Health Data Hub : un collectif critique le choix Microsoft](https://www.nextinpact.com/news/108781-health-data-hub-collectif-critique-choix-microsoft.htm)
- "Le choix d’avoir recours à l’entreprise américaine Microsoft pour l’hébergement des données, sans qu’il n’y ait eu d’appel d’offre ou de mise en concurrence préalable interpellent les acteurs du logiciel libre français"

[La France et l'Allemagne défendent un cloud souverain européen](https://www.lesechos.fr/tech-medias/hightech/le-cloud-europeen-franco-allemand-devoile-ses-services-numeriques-souverains-1208046) <em><span style="color:orange;">5 juin</span></em>
- "Avec Gaia-X, la France et l'Allemagne parrainent un catalogue d'offres de stockage et de traitement de données porté par des acteurs transparents en matière de sécurité des données."
- "catalogue de services numériques portés par des hébergeurs et des éditeurs de logiciels qui se seront préalablement engagés sur des standards de nature à renforcer la confiance de leurs clients en matière de sécurité des données mais aussi de transparence des contrats"
- "Il s'agit aussi de protéger les secrets industriels des entreprises contre les lois extraterritoriales qui autorisent, dans certains cas, la perquisition de données par des justices étrangères, américaines ou chinoises."

[Gaia-X : la France et l'Allemagne lancent leur Airbus européen du cloud](https://www.journaldunet.com/web-tech/cloud/1491859-gaia-x-la-france-et-l-allemagne-lancent-leur-airbus-europeen-du-cloud/) <em><span style="color:orange;">4 juin</span></em>

<a name="cnil"></a>
# Commission nationale de l'informatique et des libertés CNIL

[Le Conseil d’État demande au Health Data Hub des garanties supplémentaires pour limiter le risque de transfert vers les États-Unis](https://www.cnil.fr/fr/le-conseil-detat-demande-au-health-data-hub-des-garanties-supplementaires) <em><span style="color:orange;">14 octobre</span></em>
- "Dans son ordonnance du 13 octobre 2020, le Conseil d’État reconnaît l’existence d’un risque de transfert de données issues du Health Data Hub vers les États-Unis et demande des garanties supplémentaires. La CNIL conseillera les autorités publiques sur les mesures appropriées et veillera, pour l’autorisation des projets de recherche liées à la crise sanitaire, à ce que le recours à la plateforme soit réellement nécessaire."
- "Ces précautions devront être prise dans l’attente d’une solution pérenne qui permettra d’éliminer tout risque d’accès aux données personnelles par les autorités américaines, comme annoncé par le secrétaire d’État au numérique."

[Délibération no 2020-061 du 11 juin 2020 portant avis sur un projet d’arrêté fixant la liste des organismes ou services chargés d’une mission de service public pouvant mettre en œuvre des traitements de données à caractère personnel ayant pour finalité de répondre à une alerte sanitaire, dans les conditions définies à l’article 67 de la loi no 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés (demande d’avis no 20007810)](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000042071659) <em><span style="color:orange;">2  juillet</span></em>
- "[La CNIL] estime ainsi, compte tenu, d’une part, des missions attribuées à certains acteurs par les textes législatifs et réglementaires et, d’autre part, des informations transmises par le ministère, que certains organismes tels que les observatoires régionaux de santé ou encore les organismes et services chargés de la remontée des informations ou de la mise en œuvre des traitements de données (Agence du numérique en santé, Plateforme des données de santé, Agence technique de  l’information sur  l’hospitalisation) ne  semblent pas  agir en  qualité de  responsable de traitement. Ils ne devraient, dès lors, pas figurer dans l’arrêté."
- "La Commission considère que les traitements ne pourront être mis en oeuvre que dans le cadre d’une alerte sanitaire lancée par l’ANSP"
- "Un certain nombre de projets récemment soumis pour autorisation et liés à la Covid-19, qui constitue un exemple d’alerte sanitaire, ne remplissent pas les critères énoncés"

[Mesures d’organisation et de fonctionnement du système de santé nécessaires pour faire face à l’épidémie de covid-19 dans le cadre de l’état d’urgence sanitaire ](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf) <em><span style="color:orange;">20 avril</span></em>
- "Le contrat mentionne l’existence de transferts de données en dehors de l’Union européenne dans le cadre du fonctionnement courant de la plateforme"
- "Les inquiétudes […] concernant l’accès par les autorités des États-Unis aux données transférées aux États-Unis, plus particulièrement la collecte et l’accès aux données personnelles à des fins de sécurité nationale"
- "Avec des algorithmes à l’état de l’art à partir de clés générées par les responsables de la plateforme sur un boîtier chiffrant maîtrisé par la plateforme des données de santé [...]. Elles seront conservées par l’hébergeur au sein d’un boîtier chiffrant, ce qui a pour conséquence de permettre techniquement à ce dernier d’accéder aux données"
- "Eu égard à la sensibilité des données en cause, que son hébergement et les services liés à sa gestion puissent être réservés à des entités relevant exclusivement des juridictions de l’Union européenne"

[Anonymisation vs. Pseudonymisation](https://www.cnil.fr/fr/identifier-les-donnees-personnelles)

[Avis sur un projet de loi relatif à l’organisation et à la transformation du système de santé](https://www.legifrance.gouv.fr/affichCnil.do?oldAction=rechExpCnil&id=CNILTEXT000038142154&fastReqId=177029739&fastPos=1)
- "Au-delà d’un simple élargissement, cette évolution change la dimension même du SNDS, qui viserait à contenir ainsi l’ensemble des données médicales donnant lieu à remboursement"

<a name="anssi"></a>
# Agence nationale de la sécurité des systèmes d'information ANSSI

[Commission dela défense nationale et des forces armées—Audition,  à  huis  clos,  de  M.  Guillaume  Poupard,  directeur général de l’Agence nationale de la sécurité des systèmes d’information](https://media-exp1.licdn.com/dms/document/C4D1FAQE8s4mt-84ZHg/feedshare-document-pdf-analyzed/0?e=1600581600&v=beta&t=gvixXMbDd7KnVxQzquVP_iLpv_KW0KjUaI1ltQaJwSE) <em><span style="color:orange;">27 mai</span></em>
- "Bien que non déclarée, la guerre cyber, froide ou chaude,  est forte entre ennemis et alliés."
- "Les industries pharmaceutiques et les instituts de recherche sont des cibles de choix pour les grands services de renseignement."
- Concernant les données de santé : "À l’État de dire que ce sujet régalien ne concerne pas les acteurs privés"
- "Les outils de visioconférence non européens tels que Zoom par exemple, peu sécurisés et régis par des réglementations non-européennes comme le CloudAct, sont inadaptés aux échanges sensibles."
- "C’est pourquoi l’État et des industriels français ou européens coopèrent actuellement en vue de développer des systèmes capables de traiter massivement des données sans recourir à des industriels  américains comme Palantir. Nous avons montré que nous savions développer des technologies souveraines, maîtrisées, de confiance, avec des acteurs industriels accoutumés à travailler avec le ministère des armées"
- "Il faut renoncer aux solutions internationales faciles d’emploi et d’accès, et souvent moins coûteuses,  au  moins au début..."

[Sénat, audition de M. Guillaume Poupard, directeur général de l'ANSSI](https://videos.senat.fr/video.1608918_5ebaa04d6b8cd.audition-de-m-guillaume-poupard-directeur-general-de-l-agence-nationale-de-la-securite-des-systeme?timecode=4319000)  <em><span style="color:orange;">12 mai</span></em>
- "Pour insister sur les notions de souveraineté : ça m'inquiète de voir les géants du numérique avancer sur ces sujets de santé. On sait très bien qu'ils connaissent beaucoup sur nous : ce qu'on lit, nos mails, ce qu'on achète, où on se trouve...<br><br>
Si demain on leur confie en plus notre santé, il y a des conflits d’intérêt majeurs que je vois se profiler. Que se passera t-il un jour quand une société comme Apple décidera de faire de l’assurance santé. Comme ils savent tout, ils font des capteurs, des montres et ont accès à différents paramètres de santé, si on est un peu parano et je suis payé pour l’être, ils auront l’idée de faire de l’assurance santé avec évidemment ce que rêve tout assureur, assurer des gens en bonne santé et de refuser des gens qui risquent de pas être en bonne santé et qui coutent cher. Et dans ce modèle la, tout modèle mutualiste explose. A moins d’être en bonne santé on ne sera plus assuré”

[https://www.economie.gouv.fr/direct-video-conference-presse-sur-application-stopcovid-23-juin#](https://www.economie.gouv.fr/direct-video-conference-presse-sur-application-stopcovid-23-juin#) <em><span style="color:orange;">23 juin</span></em>

- "La santé publique n'est pas un sujet commercial"
- "La portabilité reste une préoccupation". "Dans une phase de lancement il est normal d'aller au plus simple, maintenant il faut enclencher la portabilité"
- "En phase opérationnelle, une solution qualifiée par l'ANSSI et non soumise a des lois extraterritoriales non européenne sera de très bon goût"

<a name="cnom"></a>
# Conseil National de l'Ordre Des Medecins CNOM
[Medecins et patients dans le monde des data, des algorithmes et de l'intelligence artificielle](https://www.conseil-national.medecin.fr/sites/default/files/external-package/edition/od6gnt/cnomdata_algorithmes_ia_0.pdf)
- Recommandation #1 :  "Une personne et une société libres et non asservies par les géants technologiques. Ce principe éthique fondamental doit être réaffirmé à l´heure où les dystopies et les utopies, les plus excessives, sont largement médiatisées"
- Recommandation #33 : "Le Cnom alerte enfin sur le fait que les infrastructures de données, plateformes de collecte et d’exploitation, constituent un enjeu majeur sur les plans scientifique, économique, et en matière de cyber-sécurité. La localisation de ces infrastructures et plateformes, leur fonctionnement, leurs finalités, leur régulation représentent un enjeu majeur de souveraineté afin que, demain, la France et l’Europe ne soient pas vassalisées par des géants supranationaux du numérique"
- "Le respect des secrets des personnes est la base même de la confiance qu’elles portent aux médecins. Il faut donc mettre cette exigence éthique dans le traitement massif des data lors de la construction des algorithmes"

<a name="conseilEtat"></a>
# Conseil d'Etat
[Conseil d'État, 19 juin 2020, Plateforme Health Data Hub](https://www.conseil-etat.fr/ressources/decisions-contentieuses/dernieres-decisions-importantes/conseil-d-etat-19-juin-2020-plateforme-health-data-hub) <em><span style="color:orange;">19 juin</span></em>

- "Dès lors, il y a lieu de prévoir que la Plateforme des données de santé complétera, dans un délai de cinq jours, les informations ainsi diffusées, pour mentionner, d'une part, le possible transfert de données hors de l'Union européenne, compte tenu du contrat passé avec son sous-traitant, et, d'autre part, les informations adaptées relatives aux droits des personnes concernées, compte tenu, le cas échéant, de l'impossibilité d'identifier ces dernières et de la nécessité du traitement pour l'exécution d'une mission d'intérêt public, envisagées aux articles 11 et 21 du règlement général sur la protection des données.""
- "La Plateforme des données de santé, d'une part, fournira à la Commission nationale de l'informatique et des libertés, dans un délai de cinq jours à compter de la notification de la présente décision, tous éléments relatifs aux procédés de pseudonymisation utilisés, propres à permettre à celle-ci de vérifier que les mesures prises assurent une protection suffisante des données de santé  traitées"

<a name="cnb"></a>
#  Conseil National des Barreaux CNB

[Motion portant sur la creation du GIP Plateforme des donnees de sante dit HealthDataHub](https://www.cnb.avocat.fr/sites/default/files/11.cnb-mo2020-01-11_ldh_health_data_hubfinal-p.pdf)
- "MET EN GARDE contre les risques de violation du secret médical et d’atteinte au droit à la vie privée"

<a name="ccne"></a>
# Comite consultatif National d'Ethique CCNE

[Données massives et santé : une nouvelle approche des enjeux éthiques](https://www.ccne-ethique.fr/sites/default/files/publications/avis_130.pdf)
- "Face au défi technologique que posent, pour la souveraineté nationale et européenne, le stockage, le partage et le traitement des données massives dans le domaine de la santé, le CCNE préconise le développement de plateformes nationales mutualisées et interconnectées. Ouvertes selon des modalités qu’il faudra définir aux acteurs publics et privés, elles doivent permettre à notre pays et à l’Europe de préserver leur autono-mie stratégique et de ne pas perdre la maîtrise de la richesse que constituent les données, tout  en privilégiant un partage contrôlé qui est indispensable à l’efficacité du soin et de la recherche médicale."

[Numérique et Santé : Quels enjeux éthiques pour quelles régulations ?](https://www.ccne-ethique.fr/sites/default/files/publications/rapport_numerique_et_sante_19112018.pdf)

<a name="loi"></a>
# Article de loi / officiel

[RAPPORT D’INFORMATION en conclusion des travaux de la mission d’évaluation et de contrôle des lois de financement de la sécurité sociale sur le dossier médical partagé et les données de santé](http://www.assemblee-nationale.fr/dyn/opendata/RINFANR5L15B3231.html#_ftnref50) <em><span style="color:orange;">22 juillet</span></em>
- "L’historique des remboursements effectués par l’assurance maladie est versé automatiquement, sauf opposition du patient, dans tout nouveau dossier médical partagé, permettant de ne plus créer de DMP vides."
- "Ainsi la rémunération sur objectifs de santé publique (ROSP) des médecins libéraux inclut désormais un critère tenant à l’utilisation d’un logiciel compatible avec le DMP. Afin de favoriser une dynamique d’ouverture, un intéressement d’un euro par dossier créé par les pharmacies a été mis en place par avenant conventionnel pour les officines."
- "Les données de santé contenues dans les DMP pourraient, en toute sécurité, être anonymisées et remontées pour alimenter le Health Data Hub"
- "La loi donne enfin une base légale à la création d’entrepôts de données constitués à partir des données issues du SNDS. Leur constitution permettra de faciliter la procédure d’instruction par la CNIL, qui n’aura plus à se prononcer sur des appariements multiples par plusieurs décisions distinctes. C’est en ce sens qu’il faut comprendre l’extension du SNDS aux données cliniques, la suppression des finalités de recherche, d’étude ou d’évaluation au profit de la finalité d’intérêt public, mais aussi l’évolution du régime juridique des données de santé lorsque le numéro d’inscription au RNIPP est utilisé en tant qu’identifiant national de santé (INS)."
- " Ainsi, à courte échéance, le dispositif d’« opt-in » sera remplacé par un dispositif d’« opt-out ». Dans le cadre de l’examen du projet de loi d’accélération et de simplification de l’action publique, le Sénat a adopté le 5 mars 2020 un amendement repoussant au 1er janvier 2022 l’ouverture automatique du DMP, afin de faire coïncider cette date avec celle de l’ouverture automatique de l’ENS. Le DMP serait alors automatiquement créé comme une composante indissociable de l’ENS. "

[Arrêté du 10 juillet 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans les territoires sortis de l'état d'urgence sanitaire et dans ceux où il a été prorogé](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000042106233&dateTexte=&categorieLien=id) <em><span style="color:orange;">11 juillet</span></em>
- "Les données ne peuvent être traitées que pour des projets poursuivant une finalité d'intérêt public en lien avec l'épidémie actuelle de covid-19 et jusqu'à l'entrée en vigueur des dispositions prises en application de l'article 41 de la loi du 24 juillet 2019 susvisée et au plus tard le 30 octobre 2020."

[Mme Nathalie GOULET : Création d'une commission d'enquête sur la protection des données de santé](http://www.senat.fr/dossier-legislatif/ppr19-576.html) <em><span style="color:orange;">26 juin</span></em>
- "Après avoir examiné les conditions de passation d'un accord confiant la gestion des données de santé française à la société Microsoft, la commission d'enquête s'attachera à élaborer des préconisations pour renforcer la souveraineté numérique française et pour assurer une gestion plus sûre des données de santé de notre système de santé et de nos concitoyens. "

[Décret n° 2020-551 du 12 mai 2020 relatif aux systèmes d'information](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000041869923&dateTexte=&categorieLien=id) <em><span style="color:orange;">12 mai</span></em>

[Arrêté : épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000041812986&dateTexte=20200421) <em><span style="color:orange;">21 avril</span></em>

[Décret n° 2017-412 du 27 mars 2017 relatif à l'utilisation du numéro d'inscription au répertoire national d'identification des personnes physiques comme identifiant national de santé, modifié en octobre 2019](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000034298418&dateTexte=20200809)
- "I.-L'identifiant national de santé défini à l'article L. 1111-8-1 est le numéro d'inscription au répertoire national d'identification des personnes physiques (NIR). "
- "L'identifiant national de santé est utilisé pour référencer les données de santé et les données administratives de toute personne bénéficiant ou appelée à bénéficier d'un acte diagnostique, thérapeutique, de prévention, de soulagement de la douleur, de compensation du handicap ou de prévention de la perte d'autonomie, ou d'interventions nécessaires à la coordination de plusieurs de ces actes."

[Naissance HealthDataHub : LOI n° 2019-774 du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038821260&categorieLien=id)

[Article 48 - RGPD - "Transferts ou divulgations non autorisés par le droit de l'Union"](https://www.privacy-regulation.eu/fr/48.htm)
- "Toute décision d'une juridiction ou d'une autorité administrative d'un pays tiers exigeant d'un responsable du traitement ou d'un sous-traitant qu'il transfère ou divulgue des données à caractère personnel ne peut être reconnue ou rendue exécutoire de quelque manière que ce soit qu'à la condition qu'elle soit fondée sur un accord international, tel qu'un traité d'entraide judiciaire, en vigueur entre le pays tiers demandeur et l'Union ou un État membre, sans préjudice d'autres motifs de transfert en vertu du présent chapitre."

[Health Data Hub mission de préfiguration](https://solidarites-sante.gouv.fr/IMG/pdf/181012_-_rapport_health_data_hub.pdf)
- "Le patrimoine de données de santé est une richesse nationale et chacun en prend peu à peu conscience.[...] La souveraineté et l´indépendance de notre système de santé face aux intérêts  étrangers, ainsi que la compétitivité de notre recherche et de notre industrie dépendront de la vitesse de la France à s´emparer du sujet."

[Confirmation de Microsoft au Sénat](http://www.senat.fr/basile/visio.do?id=qSEQ200114130&idtable=q371194%7Cq370883%7Cq370105%7Cq369641%7Cq368627%7Cq371617%7Cq371538%7Cq371754%7Cq371099%7Cq371232&_c=recherche&rch=qs&de=20170205&au=20200205&dp=3+ans&radio=dp&aff=sep&tri=dd&off=0&afd=ppr&afd=ppl&afd=pjl&afd=cvn)

[Naissance HealthDataHub : LOI n° 2019-774 du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038821260&categorieLien=id)

[Loi pour une République numérique](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033202746&categorieLien=id)
- "veiller à préserver la maîtrise, la pérennité et l’indépendance de son système d’information"
- "encourager l’utilisation des logiciels libres et des formats ouverts lors du développement, de l’achat ou de l’utilisation, de tout ou partie, de ce système d’information"

<a name="articles"></a>
# Articles scientifiques
[Estimating the success of re-identifications in incomplete datasets using generative models](https://www.nature.com/articles/s41467-019-10933-3.pdf)
- "nous constatons que 99,98% des Américains seraient correctement ré-identifiés dans n'importe quel ensemble de données en utilisant 15 attributs démographiques"

[Données personnelles : une étude enterre (définitivement) l’anonymat](https://usbeketrica.com/fr/article/donnees-personnelles-etude-enterre-anonymat)
- "83% des Américains peuvent être ré-identifiés à partir de leur genre, leur date de naissance et leur code postal. "

[The Biggest Cybersecurity Threats Are Inside Your Company](https://hbr.org/2016/09/the-biggest-cybersecurity-threats-are-inside-your-company)
- "IBM a constaté que 60% de toutes les attaques étaient perpétrées par des initiés"

<a name="doctolib"></a>
# Doctolib
[Sur Doctolib, le secret médical est soigné avec beaucoup trop de légèreté](https://www.telerama.fr/medias/sur-doctolib-le-secret-medical-est-soigne-avec-beaucoup-de-legerete-6665635.php) <em><span style="color:orange;">16 juillet</span></em>
- "le Conseil national de l’Ordre des médecins considère qu’un rendez-vous est déjà une donnée de santé, à protéger comme il se doit"
- "depuis l’an dernier, pour «sécuriser son business », comme le mentionne un document interne daté du 8 avril 2019, la licorne a recours aux data centers d’une société américaine bien connue : Amazon Web Services, la division cloud du géant."

[Mes réponses aux fausses accusations portées contre Doctolib dans l’article publié le 16 juillet 2020 par Télérama](https://medium.com/doctolib/mes-r%C3%A9ponses-aux-fausses-accusations-port%C3%A9es-contre-doctolib-dans-larticle-publi%C3%A9-le-16-juillet-b308c1e4d9fb) <em><span style="color:orange;">16 juillet</span></em>
- "Aucun salarié de Doctolib ne peut avoir accès aux données, à l’exception de 10 ingénieurs chargés de l’administration de la base de données dans le cadre d’une procédure très stricte et encadrée."


[Carte vitale biométrique : «La proposition évidente du Sénat rejetée de manière incompréhensible et contre-productive»](https://www.lefigaro.fr/economie/le-scan-eco/carte-vitale-biometrique-la-proposition-evidente-du-senat-rejetee-de-maniere-incomprehensible-et-contre-productive-20201209)

[La loi Asap est publiée au Journal officiel](https://www.lagazettedescommunes.com/711842/la-loi-asap-est-publiee-au-journal-officiel/)
- L’article 131 de la loi ajoute l’intérêt général comme motif de recours à la passation de marchés publics sans publicité ni mise en concurrence. 


[Souveraineté numérique nationale et européenne](http://www2.assemblee-nationale.fr/15/missions-d-information/missions-d-information-de-la-conference-des-presidents/souverainete-numerique-nationale-et-europeenne)

https://www-sciencesetavenir-fr.cdn.ampproject.org/c/s/www.sciencesetavenir.fr/high-tech/web/la-securite-de-zoom-mise-sous-surveillance-pour-20-ans_149807.amp

[AWS s’attaque au traitement des données de santé avec Amazon HealthLake ](https://www.usine-digitale.fr/article/aws-s-attaque-au-traitement-des-donnees-de-sante-avec-amazon-healthlake.N1038094)
- Amazon HealthLake, cette offre est une solution cloud qui permet de stocker, transformer et analyser des données de santé à l'échelle du pétaoctet. Ce nouveau service, qui fait partie intégrante d’AWS, est disponible depuis le 8 décembre.
- 

[Baromètre Wimi-Ipsos du Travail Ouvert : 2020, année zéro du télétravail dans le secteur public](https://www.ipsos.com/fr-fr/barometre-wimi-ipsos-du-travail-ouvert-2020-annee-zero-du-teletravail-dans-le-secteur-public)
- "L’Etat devrait davantage acheter de logiciels français et européens :

    91% des sondés pensent que l'Etat et le secteur public devraient réaliser au moins la moitié de leurs achats de logiciels auprès d'acteurs français (dont 41% de “tout à fait d’accord") ;
    88% pensent que les entreprises devraient effectuer au moins la moitié de leur achat auprès d’acteurs français.
    Une proportion quasi aussi importante considère que l’Etat (85%) ou les entreprises (84%) devraient s’approvisionner auprès d’entreprises européennes.
"

[Comment l’Etat favorise Amazon pour la numérisation des petites entreprises](https://up-magazine.info/societe/transition-numerique/76588-pour-letat-la-numerisation-des-entreprises-passe-par-amazon/)
- "L’État, à travers son bras armé financier, la Bpifrance, encourage donc les petites sociétés à passer le cap et à se lancer dans le numérique. Mais pas toutes seules : elle leur propose de se faire accompagner - gratuitement ! - par un géant et non des moindres, #Amazon."
- "Déjà, toutes les transactions financières liées aux prêts accordés par l’État pendant la crise sont gérées par le cloud de Jeff Bezos"
- "Il sera ainsi désormais incroyablement aisé pour Amazon de cibler par catégorie – et par secteur d’activité – les entreprises privées qui représenteraient un intérêt économique opportun pour elle. La Bpifrance facilite ainsi grandement, et de sa propre initiative, la collecte des données d’intérêt stratégique et d’intelligence économique."
- "Antoine Boulay, l’ancien directeur des relations institutionnelles et des médias de Bpifrance, a été recruté en juin dernier par Amazon Web Services, filiale spécialisée dans le stockage de données du géant américain, comme « prestataire » pour le lobbying en France."

[Voici le plan d'action de la Commission européenne pour créer "un marché unique des données" ](https://www.usine-digitale.fr/article/voici-le-plan-d-action-de-la-commission-europeenne-pour-creer-un-marche-unique-des-donnees.N1032649)
- "création "d'espaces de données à caractère personnel" qui constituent de nouveaux outils de gestion des informations."
- "Un formulaire de consentement commun sera élaboré afin de garantir le caractère non commercial de la mise à disposition."
- "Il n'y a aucune obligation de stocker et de traiter les données dans l'UE"

[Google lance une application pour faciliter l'obtention de données pour la recherche médicale ](https://www.usine-digitale.fr/article/google-lance-une-application-pour-faciliter-l-obtention-de-donnees-pour-la-recherche-medicale.N1038459)
- "Google poursuit son offensive dans la santé. La firme de Mountain View a annoncé mercredi 9 décembre 2020 déployer une nouvelle application dont le but est d'aider les chercheurs à collecter suffisamment de données de santé pour mener des études médicales. Avec Google Health Studies, les détenteurs de smartphones Android peuvent s'inscrire à des projets de recherches et répondre à des questionnaires ou fournir certaines données demandées par les chercheurs."

[Le numérique au service de notre santé : échange avec des chercheurs à l’institut Imagine. Paris Santé Campus](https://www.youtube.com/watch?v=rB9xvOSRLgw&feature=youtu.be)
- "Ils ont eu raison d’aller chercher des clouders américains. Parce qu’ils ne peuvent pas rattraper en 6 mois un retard de 10 ans !"
- "On vient de rédiger un audit par la DINUM. Dans l'avis public de la DINUM il est inscrit que le niveau que le niveau de sécurité est inégalé"

[Un arrêté précise l'organisation de la direction du numérique des ministères chargés des affaires sociales](https://www.ticsante.com/story/4944/un-arrete-precise-l-organisation-de-la-direction-du-numerique-des-ministeres-charges-des-affaires-sociales.html)
- "La Dinum est rattachée au secrétaire général du gouvernement et placée sous l'autorité du ministère de l'action et des comptes publics et pilote les directions du numérique (DNum) qui sont créées dans chaque ministère."
- "Contrairement à la délégation du numérique en santé (DNS) qui est chargée de définir et de mettre en œuvre la stratégie du numérique en santé, "les fonctions de la DNum sont tournées vers l'interne (outils pour les agents publics principalement)"

[Covid-19: un projet de décret détaille le système d'information prévu pour le suivi de la vaccination](https://www.ticsante.com/story.php?story=5483)
- "Il confie la responsabilité conjointe du SI "Vaccin Covid" à la direction générale de la santé (DGS) et à la Caisse nationale de l'assurance maladie (Cnam)."
- "la direction du numérique des ministères chargés des affaires sociales, en tant que tiers de confiance désigné par le directeur général de la santé, aux seules fins de permettre l’orientation des personnes vers un parcours de soin adapté en cas d’effets indésirables"


[La cyberattaque contre SolarWinds et Microsoft provoque la panique aux États-Unis](https://www.journaldugeek.com/2020/12/18/la-cyberattaque-contre-solarwinds-et-microsoft-provoque-la-panique-aux-etats-unis/)
- "Les pirates auraient même eu accès aux systèmes du ministère de l’Intérieur des États-Unis, et peut-être même à une partie du Pentagone et du ministère de l’Énergie qui est en charge de… l’arme nucléaire."

[SolarWinds : Microsoft confirme avoir été victime d’une faille de sécurité et que l’attaque est toujours en cours](https://siecledigital.fr/2020/12/21/solarwinds-microsoft-faille-de-securite/)
- " La Cybersecurity and Infrastructure Security Agency admet un "risque grave" pour les États-Unis. "
- "Parmi les agences sensibles touchées, nous retrouvons : le département du Trésor américain, l’Administration nationale des télécommunications et de l’information (NTIA), les Instituts nationaux de la santé (NIH), l’Agence de la cybersécurité et des infrastructures (CISA), le Département de la sécurité intérieure (DHS), le Département d’État américain, l’administration nationale de la sécurité nucléaire (NNSA), le ministère américain de l’énergie (DOE), trois États américains, ou encore la Ville d’Austin."
